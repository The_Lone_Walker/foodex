<?php 
	session_start();
    if(isset($_SESSION['nome'])){
    	if($_SESSION['Tipologia'] == "Ristorante"){
        	include 'headRest.php';
        } else if($_SESSION['Tipologia'] == "Fattorino"){
        	include 'headFattorino.php';
        } else if($_SESSION['Tipologia'] == "Admin"){
        	include 'headAdmin.php';
        } else {
			include 'head.php';
    	}
    }
    else{
    	include 'head2.php';
    }
    if(isset($_SESSION['nome'])){
?>

<h2 style="margin-top: 2%;"> Profilo </h2>
<div class="container bootstrap snippet">
    <div class="row">
    </div>
    <div class="row">
  		<div class="col-sm-3"><!--left col-->
              

<?php 
if ($_SESSION['Tipologia'] == "Ristorante"){
echo '          <div class="text-center">
        <img src="restaurantsImages/'.$_SESSION["Image"].'.jpg" class="avatar img-circle img-thumbnail" alt="avatar">
        <h6>Carica la foto profilo</h6>
        <form enctype="multipart/form-data" action="uploadImage.php" method="POST">
  			<input class=" inputfile" name="userfile" type="file" style="border: 1px solid black; margin-bottom: 2%; background-color:#ffc53a; border-radius:.25rem; padding:10px;max-width:270px">
  			<input class="btn btn-lg" type="submit" value="Invia File">
		</form>
      </div> ';
}
?>

   <!--   <hr><br>-->
  <?php 
if ($_SESSION['Tipologia'] != "Ristorante"){   
echo '          <div class="text-center">
        <img src="userImages/'.$_SESSION["Image"].'.jpg" class="avatar img-circle img-thumbnail" alt="avatar">
        <h6>Carica la foto profilo</h6>
        <form enctype="multipart/form-data" action="uploadImage.php" method="POST">
  		<input class=" inputfile" name="userfile" type="file" style="border: 1px solid black; margin-bottom: 2%; background-color:#ffc53a; border-radius:.25rem; padding:10px;max-width:270px">
  			<input class="btn btn-lg" type="submit" value="Invia File">
		</form>
      </div> ';
}
  /*     echo '     <ul class="list-group">
            <li class="list-group-item text-muted">Attività <i class="fa fa-dashboard fa-1x"></i></li>
            <li class="list-group-item text-right"><span class="pull-left"><strong>Ordini: </strong></span>';?><?php require 'countUserOrders.php';?><?php echo '</li>
          </ul> ';
}*/
?>       
        </div><!--/col-3-->
    	<div class="col-sm-9">
          <div class="tab-content">
            <div class="tab-pane active" id="infoProfile">
                <hr>
                  <form class="form" action="updateuser.php" method="post" id="registrationForm">
                  
                  <div class="form-group">
                          <div class="col-xs-6 center">
                             <label for="mobile"><h4>Nome</h4></label>
                              <input style="text-align:center;" type="text" class="form-control" name="name" id="name" title="enter your name" value="<?php echo $_SESSION['nome']; ?>" required>
                          </div>
                      </div>
                  
                      <div class="form-group"> 
                          <div class="col-xs-6 center">
                              <label for="first_name"><h4>Username</h4></label>
                              <input style="text-align:center;" type="text" class="form-control" name="username" id="username" title="enter your first name if any." value="<?php echo $_SESSION['username']; ?>" required>
                          </div>
                      </div>
                      
          
                      <div class="form-group">
                          <div class="col-xs-6 center">
                             <label for="mobile"><h4>Telefono</h4></label>
                              <input style="text-align:center;" type="text" class="form-control" name="telefono" id="telefono" title="enter your mobile number if any." value="<?php echo $_SESSION['telefono']; ?>" required>
                          </div>
                      </div>
                      <div class="form-group">
                          
                          <div class="col-xs-6 center">
                              <label for="email"><h4>Email</h4></label>
                              <input style="text-align:center;" type="email" class="form-control" name="email" id="email" title="enter your email." value="<?php echo $_SESSION['email']; ?>" required>
                          </div>
                      </div>
                      <div class="form-group">
                          
                          <div class="col-xs-6 center">
                              <label for="email"><h4>Indirizzo</h4></label>
                              <input style="text-align:center;" type="text" class="form-control" name="indirizzo" id="indirizzo" title="enter a location" value="<?php echo $_SESSION['indirizzo']; ?>" required>
                          </div>
                      </div>
                      <div class="form-group">
                          
                          <div class="col-xs-6 center">
                              <label for="password"><h4>Password</h4></label>
                              <input style="text-align:center;" type="password" class="form-control" name="password" id="password" title="enter your password" required>
                          </div>
                      </div>
                      <div class="form-group">
                          
                          <div class="col-xs-6 center">
                            <label for="password2"><h4>Conferma</h4></label>
                              <input style="text-align:center;" type="password" class="form-control" name="confirmpassword" id="confirmpassword" title="confirm your password" required>
                          </div>
                      </div>
                      <div class="form-group">
                           <div class="col-xs-12 center">
                                <br>
                              	<button class="btn btn-lg btn-success" type="submit"><em class="glyphicon glyphicon-ok-sign"></em> Salva</button>
                               	<button class="btn btn-lg" type="reset"><em class="glyphicon glyphicon-repeat"></em> Reset</button>
                            </div>
                      </div>
              	</form>
             </div><!--/tab-pane-->
            </div><!--/tab-pane-->
          </div><!--/tab-content-->
        </div><!--/col-9-->
    </div><!--/row-->
    
<?php 
	} else{   
?>

	<div class="container bootstrap snippet">
    <div class="row">
  		<div class="col-sm-10 grey-text middle-error"><h1> Errore eseguire prima il login! </h1></div> 
    </div>
    </div>
    
<?php
	}
	include 'footer.php';
?>