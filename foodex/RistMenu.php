 <?php 
 	session_start();
    if(isset($_SESSION['IDRest'])){
		include 'headRest.php';
?>

    <!-- menu -->
    <div class="content FoodMenu">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                    <div class="page-section">                     
                      <h2 class="page-title rist-food-page-title">Menu</h2>
                    </div>
                </div>
            </div>
                <!-- GET MENU RIST  -->
                        <?php 
                       	include "getMenuRist.php"; 
                        ?>             
                <!-- . GET MENU RIST -->
    	</div>
	</div>
    <!-- /.menu -->
   <input type="file" id="my_file" style="display: none;" />
   <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
      <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h4 class="modal-title" style="position:fixed;">Aggiungi Prodotto</h4>
      </div>
      <div class="modal-body">
              <form role="form" method="post" id="reused_form">
              <p>
                  Inserire i dati riguardanti il prodotto:
              </p>

              <div class="form-group">
                  <label for="title">
                      Nome Piatto:</label>
                  <input type="text" class="form-control"
                  id="title" name="title"   required maxlength="30">
              </div>
              <div class="form-group">
                  <label for="price">
                      Prezzo:</label>
                  <input type="number" class="form-control"
                  id="price" name="price" step=".01" min="0" max="10000" required>
              </div>
              <div class="form-group">
                  <label for="desc">
                      Descrizione piatto:</label>
                  <textarea class="form-control" type="textarea" name="desc"
                  id="desc" placeholder="Inserire descrizione... (max 100 caratteri)"
                  maxlength="100" rows="3"></textarea>
              </div>
              <button type="submit" class="btn btn-lg btn-success btn-block" id="btnContactUs">Conferma</button>

          </form>
          <div id="success_message" style="width:100%; height:100%; display:none; ">
              <h3>Piatto aggiunto correttamete!</h3>
          </div>
          <div id="error_message"
          style="width:100%; height:100%; display:none; ">
              <h3>Errore</h3>
              Errore inserimento prodotto.
          </div>
      </div>
      </div>

     </div>
    </div>

<?php 
	} else {
    	include 'head.php';
?>
<div class="container bootstrap snippet">
    <div class="row">
  		<div class="col-sm-10 grey-text middle-error"><h1> Errore non hai i permessi per visitare questa pagina! </h1></div> 
    </div>
    </div>
<?php 
	}
	include 'footer.php';
?>