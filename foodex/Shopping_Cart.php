<?php 
	session_start();
    if(isset($_SESSION['nome'])){
    	if($_SESSION['Tipologia'] == "Ristorante"){
        	include 'headRest.php';
        } else if($_SESSION['Tipologia'] == "Fattorino"){
        	include 'headFattorino.php';
        } else if($_SESSION['Tipologia'] == "Admin"){
        	include 'headAdmin.php';
        } else {
			include 'head.php';
    	}
    }
    else{
    	include 'head2.php';
    }
    $indice = 0;
    for($r=0; $r<count($_SESSION['Carrello']); $r++){
    	if($_SESSION['Carrello'] != -1){
        	$indice++;
        }
    }
?>

<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->
<script>

$(document).ready(function() {
 
/* Set rates + misc */ 
var fadeTime = 300; 
 
/* Assign actions */
$('.product-quantity input').change( function() {
  updateQuantity(this);
});
 
$('.product-removal button').click( function() {
  removeItem(this);
});

$('#payments').click(function() {
   console.log($("div#cart-subtotal").text());
   var price = $("div#cart-subtotal").text();
   var desc = "";

   $('.product').each(function () {
    desc = desc+$(this).children('.product-title').text()+", ";
   });   
   window.open('orderedAndUpdate.php?price='+price+'&desc='+desc);
});
 
/* Recalculate cart */
function recalculateCart()
{
  var subtotal = 0.0;
   
  /* Sum up row totals */
  $('.product').each(function () {
    subtotal += parseFloat($(this).children('.product-line-price').text());
  });
   
  /* Calculate totals */
  var total = subtotal;
   
  /* Update totals display */
  $('.totals-value').fadeOut(fadeTime, function() {
    $('#cart-subtotal').html(subtotal.toFixed(2));
    $('#cart-total').html(total.toFixed(2));
    if(total == 0){
      $('.checkout').fadeOut(fadeTime);
    }else{
      $('.checkout').fadeIn(fadeTime);
    }
    $('.totals-value').fadeIn(fadeTime);
  });
}
 
 
/* Update quantity */
function updateQuantity(quantityInput)
{
  /* Calculate line price */
  var productRow = $(quantityInput).parent().parent();
  var price = parseFloat(productRow.children('.product-price').text());
  var quantity = $(quantityInput).val();
  var linePrice = price * quantity;
   
  /* Update line price display and recalc cart totals */
  productRow.children('.product-line-price').each(function () {
    $(this).fadeOut(fadeTime, function() {
      $(this).text(linePrice.toFixed(2));
      recalculateCart();
      $(this).fadeIn(fadeTime);
    });
  });  
}

/* Remove item from cart */
function removeItem(removeButton)
{
  /* Remove row from DOM and recalc cart total */
  var productRow = $(removeButton).parent().parent();
  productRow.slideUp(fadeTime, function() {
    productRow.remove();
    recalculateCart();
  });
}
 
});
</script>
<style>
.product-description {
font-size:10px;
width:150px;
}
</style>
<div class="shopping-cart container">
 
<table id="cart" class="table table-hover table-condensed">
<thead>
<tr>
<th style="width: 50%;"><label class="product-details">Prodotti</label></th>
<th class="center" style="width: 10%;"><label class="product-price">Prezzo</label></th>
<th style="width: 1%;"><label class="product-quantity">Quantità</label></th>
<th style="width: 10%;"><label class="product-removal"></label></th>
<th class="center"style="width: 22%;"><label class="product-line-price">Totale</label></th>
</tr>
</thead>
<tbody>
<?php
include 'fillShoppingCart.php';
?>
</tbody>
<tfoot>
<?php
if($indice!=0) echo' 
<tr>
<td>Seleziona il luogo di consegna:</td>
<td><select id="deliverySelect" onchange="updateDeliveryAddress(\'deliverySelect\')">  
<option value="IngressoPrimoPiano">Ingresso primo piano</option>
<option value="IngressoPianoTerra">Ingresso piano terra</option>
<option value="Blocco A">Blocco A</option>
<option value="Blocco B">Blocco B</option>
<option value="Blocco C">Blocco C</option>
<option value="Blocco D">Blocco D</option></select></td>
</tr>';
?>
<tr>
<td><a class="btn btn-warning" href="restaurantsList.php">Acquista altro</a></td>
<td class="hidden-xs" colspan="2">&nbsp;</td>
<td></td>
<td><form action="payment.php"><input type="submit" class="btn btn-success btn-block checkout" value="Procedi al pagamento" <?php if($indice==0) echo "disabled";?>/></form></td>
</tr>
</tfoot>
</table>
</div>
<?php 
	include 'footer.php';
?>