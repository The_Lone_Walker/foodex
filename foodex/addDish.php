<?php
session_start();

$servername="localhost";
$username ="foodex";
$password ="";
$database = "my_foodex";

$ID = $_POST['idcat'];
$TITLE = $_POST['Title'];
$DESC = $_POST['Description'];
$PRICE = $_POST['Price'];
if(!isset($IMG)){
	$IMG = "http://via.placeholder.com/70x70";
}
if(isset($ID) && $ID != "" && isset($TITLE) && $TITLE != "" && isset($DESC) && isset($PRICE) && $PRICE != ""){
  	$con = new mysqli($servername, $username, $password, $database);
    $stmt = $con->prepare("INSERT INTO FOOD(Name, Description, Price, Categoria) VALUES (?, ?, ?, ?)");
    $stmt->bind_param("ssdi", $TITLE, $DESC, $PRICE, $ID);
    $result = $stmt->execute();
    if($result){
    	$lastid = $con->insert_id;
      echo '
<div data-id="' . $lastid . '" class="menu-content MenuHover">
	<div class="row">
	   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
		  <div class="dish-img rist-img-editable" data-id="' . $lastid . '"><a href="#"><img id="img' . $lastid . '" src="' . $IMG . '" alt="" class="img-circle "></a></div>
	   </div>
	   <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
		  <div class="dish-content">
			 <p class="hidden dish-edit-text">Nome</p>
			 <input type="text" class="edit-input edit-menuelement edit-menuelement-title"  value="' . $TITLE . '"/>
			 <h5 class="dish-title dish-title-rist"><a>' . $TITLE .' </a></h5>
			 <p class="hidden dish-edit-text">Descrizione</p>
			 <span class="dish-meta">' . $DESC . '</span>
			 <input type="text" class="edit-input edit-menuelement edit-menuelement-description" value="' . $DESC . '"/>
			 <div class="dish-price">
				<p class="hidden dish-edit-text">Prezzo</p>
				<input type="text" class="edit-input edit-menuelement edit-menuelement-price" value="' . $PRICE . '"/>
				<p class="dish-price-p">' . number_format((float)$PRICE, 2, '.', '') .' €</p>
			 </div>
		  </div>
		  <div data-id="' . $lastid . '" class="MenuRemove">
			  <svg xmlns="http://www.w3.org/2000/svg" class="glyph si-glyph-trash">
				 <title>Elimina Prodotto</title>
				 <use xlink:href="sprite.svg#si-glyph-trash" />
			  </svg>
		   </div>
		   <div class="edit-accept edit-accept-menuelement edit" data-idDish="' . $lastid .'">
			   <svg xmlns="http://www.w3.org/2000/svg" class="glyph si-glyph-checked">
				  <title>Applica Modifiche</title>
				  <use xlink:href="sprite.svg#si-glyph-checked" />
			   </svg>
			</div>
			<div class="edit-undo edit-undo-menuelement edit">
			   <svg xmlns="http://www.w3.org/2000/svg" class="glyph si-glyph-delete">
				  <title>Annulla Modifiche</title>
				  <use xlink:href="sprite.svg#si-glyph-delete" />
			   </svg>
			</div>
		</div>
	</div>
</div>';
    } else {
      die(header("ERRORE"));
    }

    $stmt->close();
    $con->close();
} else{
	die(header("ERRORE"));
}
?>