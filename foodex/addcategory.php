<?php
session_start();

$servername="localhost";
$username ="foodex";
$password ="";
$database = "my_foodex";

$ID = $_GET['idRest'];
$NOME = $_GET['Nome'];

if(isset($NOME)){
    $con = new mysqli($servername, $username, $password, $database);
    
    $stmt = $con->prepare("INSERT INTO CATEGORIA(Nome, IdRistorante) VALUES (?, ?)");
    $stmt->bind_param('si', $NOME , $ID);
    $stmt->execute();
    if (!$stmt) {
    	header("location: RistMenu.php?msg=ErrorCategory");
	} else {
    	header("location: RistMenu.php?msg=CategoryAdded");
    }

  $stmt->close();
  $conn->close();
}
?>