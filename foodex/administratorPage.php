<?php 
	session_start();
    if(isset($_SESSION['nome'])){
    	if($_SESSION['Tipologia'] == "Ristorante"){
        	include 'headRest.php';
        } else if($_SESSION['Tipologia'] == "Fattorino"){
        	include 'headFattorino.php';
        } else if($_SESSION['Tipologia'] == "Admin"){
        	include 'headAdmin.php';
        } else {
			include 'head.php';
    	}
    }
    else{
    	include 'head2.php';
    }
    if(isset( $_SESSION['nome']) and $_SESSION['Tipologia'] == "Admin")
    {
?>

<h2 style ="margin-top:2%;">Ristoranti</h2>
<table id="example1" class="administratorTable table table-striped table-bordered dt-responsive nowrap" style="width:100%; margin-top:0.5%;">
        <thead>
            <tr>
                <th class="center" scope ="col" id="ID">IDRistorante</th>
                <th class="center" scope ="col" id="Name">Nome</th>
                <th class="center" scope ="col" id="Username">Username</th>
                <th class="center" scope ="col" id="Address">Indirizzo</th>
                <th class="center" scope ="col" id="Email">Email</th>
                <th class="center" scope ="col" id="Phone">Telefono</th>
                <th class="center" scope ="col" id="Remove">Rimuovi</th>
            </tr>
        </thead>
        <tbody>
       					<?php
					require 'getAllRestaurant.php';
					?>
        </tbody>
    </table>
    
    <form method="get" action="addRestaurant.php">
    <table id="example2" class="administratorTable table table-striped table-bordered dt-responsive nowrap" style="width:100%;margin-top:0.5%;">
        <thead>
            <tr>            
                <th class="center" scope ="col" id="ID1">Nome</th>
                <th class="center" scope ="col" id="Username1">Username</th>
                <th class="center" scope ="col" id="Password1">Password</th>
                <th class="center" scope ="col" id="Address1">Indirizzo</th>
                <th class="center" scope ="col" id="Email1">Email</th>
                <th class="center" scope ="col" id="Phone1">Telefono</th>
                <th class="center" scope ="col" id="Categorie1">Categorie</th>
                <th class="center" scope ="col" id="ADD1">Aggiungi</th>
            </tr>
        </thead>
        <tbody>
        <tr>     
		<th class="center" scope ="row" id="NameInsert"><label for="name4" style="display:none;">Name</label><input type="text" id="name4" name="Name" required /></th>
        <td class="center" headers="Username1 NameInsert"><label for="username4" style="display:none;">Username</label><input type="text" id="username4" name="Username" required/></td>
        <td class="center" headers="Password1 NameInsert"><label for="password4" style="display:none;">Password</label><input type="text" id="password4" name="Password" required/></td>
        <td class="center" headers="Address1 NameInsert"><label for="address4" style="display:none;">Address</label><input type="text" id="address4" name="Address" required/></td>
        <td class="center" headers="Email1 NameInsert"><label for="email4" style="display:none;">Email</label><input type="text" id="email4" name="Email" required/></td>
        <td class="center" headers="Phone1 NameInsert"><label for="phone4" style="display:none;">Phone</label><input type="text" id="phone4" name="Phone" required/></td>
        <td class="center" headers="Categorie1 NameInsert"><label for="categorie4" style="display:none;">Categorie</label><select multiple size="2" name="Categorie[]" id="categorie4" ><?php require 'getAllCategories.php';?></select></td>
        <td class="center" headers="ADD1 NameInsert"><button type="submit" class="btn btn-lg" style="width:100%">Aggiungi</button></td>
        </tr>
        </tbody>
    </table>
    </form>
    

<h2 style ="margin-top:2%;">Utenti</h2>
<table id="example3" class="administratorTable table table-striped table-bordered dt-responsive nowrap" style="width:100%;margin-top:0.5%;">
        <thead>
            <tr>
                <th class="center" scope ="col" id="Name2">Nome</th>
                <th class="center" scope ="col" id="Username2">Username</th>
                <th class="center" scope ="col" id="Email2">Email</th>
                <th class="center" scope ="col" id="Address2">Indirizzo</th>
                <th class="center" scope ="col" id="Phone2">Telefono</th>
                <th class="center" scope ="col" id="Type2"> Tipo Account</th>
                <th class="center" scope ="col" id="BAN2">Rimuovi</th>
            </tr>
        </thead>
        <tbody>
       					<?php
					require 'getAllUsers.php';
					?>
        </tbody>
    </table>

<?php 
	} else{   
?>
	<div class="container bootstrap snippet">
    <div class="row">
  		<div class="col-sm-10 grey-text middle-error"><h1> Non possiedi i permessi per accedere alla seguente pagina! </h1></div> 
    </div>
    </div>  
<?php
	}
	include 'footer.php';
?>