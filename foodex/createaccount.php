<?php

session_start();

$servername="localhost";
$username ="foodex";
$password ="";
$database = "my_foodex";

$nome = $_POST['name'];
$email = $_POST['email'];
$user = $_POST['username'];
$pass = $_POST['password'];
$ind = $_POST['indirizzo'];
$tel = $_POST['telefono'];
$password = password_hash($pass, PASSWORD_DEFAULT);

if(isset($email) and isset($user) and isset($pass) and isset($ind) and isset($tel)){

	$con = new mysqli($servername, $username, $password, $database);
    
    $sql = ("INSERT INTO UTENTE (Nome, Username, Password, Email, Indirizzo, Telefono) VALUES(?, ?, ?, ?, ?, ?)");

	if($stmt = $con->prepare($sql)) {
    
    $a = 2;
   	 $stmt->bind_param('ssssss', $nome, $user, $password, $email, $ind, $tel);
     if($stmt->execute()){
     	$_SESSION['nome'] = $nome;
        $_SESSION['email'] = $email;
        $_SESSION['username'] = $user;
        $_SESSION['indirizzo'] = $ind;
        $_SESSION['telefono'] = $tel;
		$_SESSION['Tipologia'] = "Utente";
        $_SESSION['Carrello'] = array();
       $_SESSION['Quantity'] = array();
       $_SESSION["RistorantiDiAppartenenza"] = array();
       $_SESSION['ShoppingPrice'] = array();
       $_SESSION['LuogoConsegna'] = "Blocco A"; 
    	header("location: http://foodex.altervista.org/Profile.php");
     } else {
     	echo $stmt->error;
        header("location: index.php");
     }
    } else {
    	$error = $con->errno . ' ' . $con->error;
    	echo $error;
        header("location: index.php");
    }
}
?>
