<?php
session_start();

$servername="localhost";
$username ="foodex";
$password ="";
$database = "my_foodex";


    $con = new mysqli($servername, $username, $password, $database);
    
    $stmt = $con->prepare("DELETE FROM UTENTE WHERE Username = ?");
    $stmt->bind_param('s', $_GET['Username']);
    $stmt->execute();
    $result = $stmt->get_result();
    if($_SESSION["Tipologia"] == "Admin"){
    	header("location: administratorPage.php");
    } else {
    	header("location: gestioneFattorini.php");
    }
?>