<?php
session_start();
$servername="localhost";
$username ="foodex";
$password ="";
$database = "my_foodex";

	


    $con = new mysqli($servername, $username, $password, $database);
    for($i = 0; $i < count($_SESSION["Carrello"]); $i++){
	if($_SESSION['Carrello'][$i] == -1){
		continue;    
    }
    $sql = ("SELECT * FROM FOOD WHERE IdFood = ?");
    if($stmt = $con->prepare($sql)){
	$food = str_replace("Item","",$_SESSION['Carrello'][$i]);
    $stmt->bind_param('i',$food);
    if($stmt->execute()){ 
    $result = $stmt->get_result();
   	if($row = $result->fetch_array()) {
       echo '<tr>
<td data-th="Prodotto">
<div class="product row ">
    <div class="product-details col-sm-10">
      <h4 class="product-title">'.$row["Name"].'</h4>
      <p class="product-description">'.$row["Description"].'</p>
    </div>
</div>
    <td data-th="Prezzo" class="product-price text-center">'.$row["Price"].' €</td>
    <td data-th="Quantità" class="product-quantity text-center">'.$_SESSION["Quantity"][$i].'</td>
    <td class="product-removal btn-center">
      <button class="remove-product btn btn-warning " onclick="removeFromCart(\'Item'.$row["IdFood"].'\')">
        Rimuovi
      </button>
    </td>
    <td data-th="Totale" id="valueCart" class="product-line-price text-center">'.$row["Price"] * $_SESSION["Quantity"][$i] .'</td>
</tr>';      
    }
    } else {
   		echo $stmt->error;
    }
    } else {
        $error = $con->errno . ' ' . $con->error;
    	echo $error;
    }
    }
    
?>