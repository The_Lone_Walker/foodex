<?php 
	session_start();
    if(isset($_SESSION['nome'])){
    	if($_SESSION['Tipologia'] == "Ristorante"){
        	include 'headRest.php';
        } else if($_SESSION['Tipologia'] == "Fattorino"){
        	include 'headFattorino.php';
        } else if($_SESSION['Tipologia'] == "Admin"){
        	include 'headAdmin.php';
        } else {
			include 'head.php';
    	}
    }
    else{
    	include 'head2.php';
    }
    $title = "GESTIONE FATTORINI";
    $buffer = preg_replace('/(<title>)(.*?)(<\/title>)/i', '$1' . $title . '$3', $buffer);
    echo $buffer;
    if(isset( $_SESSION['nome']) and $_SESSION['Tipologia'] == "Ristorante")
    {
?>

<h2 style ="margin-top:2%;">Fattorini</h2>
<table id="example" class="workManageTable table table-striped table-bordered dt-responsive nowrap" style="width:100%; margin-top:0.5%;">
        <thead>
            <tr>
                <th class= "center" scope="col" id="Name">Name</th>
                <th class= "center" scope="col" id="Username">Username</th>
                <th class= "center" scope="col" id="Email">Email</th>
                <th class= "center" scope="col" id="Address">Address</th>
                <th class= "center" scope="col" id="Phone">Phone</th>
                <th class= "center" scope="col" id="Licenzia">Licenzia</th>
            </tr>
        </thead>
        <tbody>
       					<?php
					require 'getAllWorkers.php';
					?>
        </tbody>
    </table>
    
<?php 
	} else{   
?>
	<div class="container bootstrap snippet">
    <div class="row">
  		<div class="col-sm-10 grey-text middle-error"><h1> Non possiedi i permessi per accedere alla seguente pagina! </h1></div> 
    </div>
    </div>  
<?php
	}
	include 'footer.php';
?>