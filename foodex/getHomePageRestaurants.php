<?php
session_start();

$servername="localhost";
$username ="foodex";
$password ="";
$database = "my_foodex";
	
    $con = new mysqli($servername, $username, $password, $database);
    $sql = ("SELECT * FROM RESTAURANT LIMIT 9");
    
    if($stmt = $con->prepare($sql)){
    	if($stmt->execute()){
       		 $result = $stmt->get_result();
       		 $res = mysqli_fetch_all($result,MYSQLI_ASSOC);
   			 //print_r($res);
             
             echo'
             
             
             <!-- INIZIO GALLERIAAAA -->
<div class="container-fluid topPadding bg1">
<h2>I Nostri Ristoranti</h2>
<!-- Grid row -->
<div class="row topMargin">

  <!-- Grid column -->
  <div class="col-lg-4 col-md-12 mb-4">

    <!--Modal: Name-->
    <div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
      <div class="modal-dialog modal-lg" id="myModalLabel1" role="document">

        <!--Content-->
        <div class="modal-content">

          <!--Body-->
          <div class="modal-body mb-0 p-0">

            <div class="embed-responsive embed-responsive-16by9 z-depth-1-half">
              <img class="img-fluid" alt="'. $res[0]["Name"] .'" src="'. 'restaurantsImages/'.$res[0]["Image"].'.jpg' .'">
            </div>

          </div>

          <!--Footer-->
          <div class="modal-footer justify-content-center">
            <a class="btn btn-outline-primary btn-rounded btn-md " href="menuRist.php?idrist='.$res[0]["IdRestaurant"].'" role="button">Mostra</a>

          </div>

        </div>
        <!--/.Content-->

      </div>
    </div>
    <!--Modal: Name-->

    <a><img class="img-fluid z-depth-1" src="'. 'restaurantsImages/'.$res[0]["Image"].'.jpg' .'" alt="'. $res[0]["Name"] .'"
        data-toggle="modal" data-target="#modal1"></a>
    <p class = "description">' . $res[0]["Name"] . '</p>
  </div>
  <!-- Grid column -->

  <!-- Grid column -->
  <div class="col-lg-4 col-md-6 mb-4">

    <!--Modal: Name-->
    <div class="modal fade" id="modal6" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
      <div class="modal-dialog modal-lg" id="myModalLabel2" role="document">

        <!--Content-->
        <div class="modal-content">

          <!--Body-->
          <div class="modal-body mb-0 p-0">

            <div class="embed-responsive embed-responsive-16by9 z-depth-1-half">
              <img class="img-fluid" alt="'. $res[1]["Name"] .'" src="'. 'restaurantsImages/'.$res[1]["Image"].'.jpg' .'">
            </div>

          </div>

          <!--Footer-->
          <div class="modal-footer justify-content-center">
			<a class="btn btn-outline-primary btn-rounded btn-md " href="menuRist.php?idrist='.$res[1]["IdRestaurant"].'" role="button">Mostra</a>

          </div>

        </div>
        <!--/.Content-->

      </div>
    </div>
    <!--Modal: Name-->

    <a><img class="img-fluid z-depth-1" src="'. 'restaurantsImages/'.$res[1]["Image"].'.jpg' .'" alt="'. $res[1]["Name"] .'"
        data-toggle="modal" data-target="#modal6"></a>
	<p class = "description">' . $res[1]["Name"] . '</p>
  </div>
  <!-- Grid column -->

  <!-- Grid column -->
  <div class="col-lg-4 col-md-6 mb-4">

    <!--Modal: Name-->
    <div class="modal fade" id="modal4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
      <div class="modal-dialog modal-lg" id="myModalLabel3" role="document">

        <!--Content-->
        <div class="modal-content">

          <!--Body-->
          <div class="modal-body mb-0 p-0">

            <div class="embed-responsive embed-responsive-16by9 z-depth-1-half">
              <img class="img-fluid" alt="'. $res[2]["Name"] .'" src="'. 'restaurantsImages/'.$res[2]["Image"].'.jpg' .'">
            </div>

          </div>

          <!--Footer-->
          <div class="modal-footer justify-content-center">
			<a class="btn btn-outline-primary btn-rounded btn-md " href="menuRist.php?idrist='.$res[2]["IdRestaurant"].'" role="button">Mostra</a>

          </div>

        </div>
        <!--/.Content-->

      </div>
    </div>
    <!--Modal: Name-->

    <a><img class="img-fluid z-depth-1" src="'. 'restaurantsImages/'.$res[2]["Image"].'.jpg' .'" alt="'. $res[2]["Name"] .'"
        data-toggle="modal" data-target="#modal4"></a>
	<p class = "description">' . $res[2]["Name"] . '</p>
  </div>
  <!-- Grid column -->

</div>
<!-- Grid row -->

<!-- Grid row -->
<div class="row">

  <!-- Grid column -->
  <div class="col-lg-4 col-md-12 mb-4">

    <!--Modal: Name-->
    <div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel4" aria-hidden="true">
      <div class="modal-dialog modal-lg" id="myModalLabel4" role="document">

        <!--Content-->
        <div class="modal-content">

          <!--Body-->
          <div class="modal-body mb-0 p-0">

            <div class="embed-responsive embed-responsive-16by9 z-depth-1-half">
              <img class="img-fluid" alt="'. $res[3]["Name"] .'" src="'. 'restaurantsImages/'.$res[3]["Image"].'.jpg' .'">
            </div>

          </div>

          <!--Footer-->
          <div class="modal-footer justify-content-center">
			<a class="btn btn-outline-primary btn-rounded btn-md " href="menuRist.php?idrist='.$res[3]["IdRestaurant"].'" role="button">Mostra</a>

          </div>

        </div>
        <!--/.Content-->

      </div>
    </div>
    <!--Modal: Name-->

    <a><img class="img-fluid z-depth-1" src="'. 'restaurantsImages/'.$res[3]["Image"].'.jpg' .'" alt="'. $res[3]["Name"] .'"
        data-toggle="modal" data-target="#modal2"></a>
	<p class = "description">' . $res[3]["Name"] . '</p>
  </div>
  <!-- Grid column -->

  <!-- Grid column -->
  <div class="col-lg-4 col-md-6 mb-4">

    <!--Modal: Name-->
    <div class="modal fade" id="modal5" tabindex="-1" role="dialog" aria-labelledby="myModalLabel5" aria-hidden="true">
      <div class="modal-dialog modal-lg" id="myModalLabel5" role="document">

        <!--Content-->
        <div class="modal-content">

          <!--Body-->
          <div class="modal-body mb-0 p-0">

            <div class="embed-responsive embed-responsive-16by9 z-depth-1-half">
              <img class="img-fluid" alt="'. $res[4]["Name"] .'" src="'. 'restaurantsImages/'.$res[4]["Image"].'.jpg' .'">
            </div>

          </div>

          <!--Footer-->
          <div class="modal-footer justify-content-center">
			<a class="btn btn-outline-primary btn-rounded btn-md " href="menuRist.php?idrist='.$res[4]["IdRestaurant"].'" role="button">Mostra</a>

          </div>

        </div>
        <!--/.Content-->

      </div>
    </div>
    <!--Modal: Name-->

    <a><img class="img-fluid z-depth-1" src="'. 'restaurantsImages/'.$res[4]["Image"].'.jpg' .'" alt="'. $res[4]["Name"] .'"
        data-toggle="modal" data-target="#modal5"></a>
	<p class = "description">' . $res[4]["Name"] . '</p>
  </div>
  <!-- Grid column -->

  <!-- Grid column -->
  <div class="col-lg-4 col-md-6 mb-4">

    <!--Modal: Name-->
    <div class="modal fade" id="modal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel6" aria-hidden="true">
      <div class="modal-dialog modal-lg" id="myModalLabel6" role="document">

        <!--Content-->
        <div class="modal-content">

          <!--Body-->
          <div class="modal-body mb-0 p-0">

            <div class="embed-responsive embed-responsive-16by9 z-depth-1-half">
              <img class="img-fluid" alt="'. $res[5]["Name"] .'" src="'. 'restaurantsImages/'.$res[5]["Image"].'.jpg' .'">
            </div>

          </div>

          <!--Footer-->
          <div class="modal-footer justify-content-center">
			<a class="btn btn-outline-primary btn-rounded btn-md " href="menuRist.php?idrist='.$res[5]["IdRestaurant"].'" role="button">Mostra</a>

          </div>

        </div>
        <!--/.Content-->

      </div>
    </div>
    <!--Modal: Name-->

    <a><img class="img-fluid z-depth-1" src="'. 'restaurantsImages/'.$res[5]["Image"].'.jpg' .'" alt="'. $res[5]["Name"] .'"
        data-toggle="modal" data-target="#modal3"></a>
<p class = "description">' . $res[5]["Name"] . '</p>
  </div>
  <!-- Grid column -->

</div>
<!-- Grid row -->
</div>

             
             
             ';
             
             
   		 } else {
        	 echo $stmt->error;
    	}
	} else {
   		 	$error = $con->errno . ' ' . $con->error;
    		echo "Errore query " . $error;
    }
?>