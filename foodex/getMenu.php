 <?php
$servername="localhost";
$username ="foodex";
$password ="";
$database = "my_foodex";
$idRist = 2;

// Create connection
$conn = new mysqli($servername, $username, $password, $database);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT * FROM FOOD WHERE Categoria = ".$idRist."";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
 	echo "<tr>";
    // output data of each row
    while($row = $result->fetch_assoc()) {
    	echo "<td data-th=\"Product\"> 
        		<div class=\"row\"> 
            		<div class=\"col-sm-2 hidden-xs\"> </div> 
                		<div class=\"col-sm-10\">";
        echo "<h4 class=\"nomargin\">" . $row["Name"] . "</h4>";
        echo "<p>" . $row["Description"] . "</p> 
        		</div>
      		</div>
 		</td>";
        echo "<td data-th=\"Price\">" . $row["Price"] . "</td>
		<td data-th=\"Quantity\"><input class=\"form-control text-center\" type=\"number\" value=\"1\" /></td>
		<td class=\"text-center\" data-th=\"Subtotal\">" . $row["Price"] ."</td>
		<td class=\"actions\" data-th=\"\">&nbsp;</td>
		</tr>";
    }
    echo "</tr>";
} else {
    echo "<p> Questo ristorante non ha ancora aggiunto prodotti al menu. </p>";
}
$conn->close();
?> 