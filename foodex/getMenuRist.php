 <?php
$servername="localhost";
$username ="foodex";
$password ="";
$database = "my_foodex";

$idR = $_SESSION["IDRest"];

// Create connection
$conn = new mysqli($servername, $username, $password, $database);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
$sql = "SELECT * FROM FOOD RIGHT JOIN CATEGORIA ON (Categoria = IdCategoria) WHERE IdRistorante = " . $idR . " ORDER BY Categoria, IdFood";
$result = $conn->query($sql);
$i = 0;
if ($result->num_rows > 0) {
	$oldCategoria = null;
    echo '<div class="row">';
	while($row = $result->fetch_assoc()) {
      $idCategoria = $row["IdCategoria"];
      if($idCategoria != $oldCategoria){
    	$i= $i + 1;
      	if($oldCategoria != null){         
          echo '
                <div class="menu-content">
                  <div class="row">
                      <div data-idCat="' . $oldCategoria .'" data-toggle="modal" data-target="#myModal" class="type-1">
                          <a class="btnAddElementMenu btn btn-mm btn-2 tabbable" tabindex="2">
                              <span class="txt">Aggiungi</span>
                              <span class="round"><i class="fa fa-chevron-right"></i></span>
                          </a>
                      </div>
                  </div>
              </div>
          	</div>
          </div>';
        }
      	$oldCategoria = $idCategoria;
        
        echo '
			<!--Nuova Categoria-->
        	<div class="col-md-4 mb40">
                    <div class="menu-block">
                        <h3 class="menu-title">' . $row["Nome"] .'</h3>
    					<input title="Nuovo nome categoria" type="text" class="edit-input-title edit-input" />
                        <div class="edit-accept edit-title-accept edit tabbable" tabindex="2" data-idCat="' . $row["IdCategoria"] . '">
                           <svg xmlns="http://www.w3.org/2000/svg" class="glyph si-glyph-checked">
                           	  <title>Applica Modifiche</title>
                              <use xlink:href="sprite.svg#si-glyph-checked" />
                          </svg> 
                        </div>
                        <div class="edit-undo edit-undo-title edit tabbable" tabindex="2">
                           <svg xmlns="http://www.w3.org/2000/svg" class="glyph si-glyph-delete">
                           	  <title>Annulla Modifiche</title>
                              <use xlink:href="sprite.svg#si-glyph-delete" />
                          </svg> 
                        </div>
                        <div class="edit_menutitle edit tabbable" tabindex="2">
                           <svg xmlns="http://www.w3.org/2000/svg" class="glyph si-glyph-document-edit">
                           	  <title>Modifica Nome Categoria</title>
                              <use xlink:href="sprite.svg#si-glyph-document-edit" />
                          </svg> 
                        </div>
                        <div data-id="' . $row["IdCategoria"] . '" tabindex="2" class="CategoryRemove tabbable">
            				<svg xmlns="http://www.w3.org/2000/svg" class="glyph si-glyph-trash"><title>Elimina Categoria</title><use xlink:href="sprite.svg#si-glyph-trash" /></svg>
            			</div>';
      }
      if(isset($row["IdFood"])){
        echo '<div data-id="' . $row["IdFood"] . '" class="menu-content MenuHover">
          <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
              <div class="dish-img rist-img-editable" data-id="' . $row["IdFood"] . '"><a href="#"><img id="img' . $row["IdFood"] . '" src="' . $row["Image"] . '" tabindex="2" alt="Immagine piatto" class="img-circle tabbable"></a></div>
              ';
              if($row["Image"] != "http://via.placeholder.com/70x70"){
              		echo '<p class="remove-img">Rimuovi</p>';
              }
              echo '</div>
              <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                <div class="dish-content">
                  <p class="hidden dish-edit-text">Nome</p>
                  <input title="Nuovo nome piatto" type="text" class="edit-input edit-menuelement edit-menuelement-title"  value="' . $row["Name"] . '"/>
                  <h5 class="dish-title dish-title-rist tabbable" tabindex="2"><a>' . $row["Name"] .'</a></h5>
                  <p class="hidden dish-edit-text">Descrizione</p>
                  <span class="dish-meta">' . $row["Description"] . '</span>
                  <input title="Nuova descrizione piatto" type="text" class="edit-input edit-menuelement edit-menuelement-description" value="' . $row["Description"] . '"/>
                  <div class="dish-price">          
                    <p class="hidden dish-edit-text">Prezzo</p>
                    <input title="Nuovo prezzo piatto" type="text" class="edit-input edit-menuelement edit-menuelement-price" value="' . $row["Price"] . '"/>
                    <p class="dish-price-p">' . $row["Price"] .' €</p>
                  </div>
                </div>
              <div data-id="' . $row["IdFood"] . '" tabindex="2" class="MenuRemove tabbable">
                  <svg xmlns="http://www.w3.org/2000/svg" class="glyph si-glyph-trash"><title>Elimina Prodotto</title><use xlink:href="sprite.svg#si-glyph-trash" /></svg>
              </div>
              <div class="edit-accept edit-accept-menuelement edit tabbable" tabindex="2" data-idDish="' .$row["IdFood"] .'">
                  <svg xmlns="http://www.w3.org/2000/svg" class="glyph si-glyph-checked">
                      <title>Applica Modifiche</title>
                      <use xlink:href="sprite.svg#si-glyph-checked" />
                  </svg> 
              </div>
              <div class="edit-undo edit-undo-menuelement edit tabbable" tabindex="2">
                  <svg xmlns="http://www.w3.org/2000/svg" class="glyph si-glyph-delete">
                      <title>Annulla Modifiche</title>
                      <use xlink:href="sprite.svg#si-glyph-delete" />
                  </svg> 
             </div>
            </div>
          </div>
        </div>';
        }
      }
    echo '	<div class="menu-content">
                  <div class="row">
                      <div data-idCat="' . $oldCategoria .'" data-toggle="modal" data-target="#myModal" class="type-1">
                          <a class="btnAddElementMenu btn btn-mm btn-2 tabbable" tabindex="2">
                              <span class="txt">Aggiungi</span>
                              <span class="round"><i class="fa fa-chevron-right"></i></span>
                          </a>
                      </div>
                  </div>
              </div>';
    echo'		</div>
          </div>';
    if( ($i % 4) == 0){ 
      echo '<div id="MenuAddNew" class="col-md-4 mb40">
                     <div class="form-popup" id="FormCateg">
                        <form action="addcategory.php" class="form-container">
                           <input type="hidden" name="idRest" value="' . $idR . '">
                           <label for="Nome"><strong>Categoria</strong></label>
                           <input title="Nome piatto" type="text" placeholder="Inserire nome" name="Nome" required>
                           <button type="submit" class="btn">Conferma</button>
                           <button type="button" id="closeCatForm" class="btn cancel">Annulla</button>
                        </form>
                     </div>
                     <div class="menu-block tabbable" tabindex="2" id="divCatAddNew">
                        <p class="text-center align-middle PlusAddNew">&#43;</p>
                     </div>
             </div>';
    	} else{
        	echo '<div id="MenuAddNew" class="col mb40" style="width:100%;">
                     <div class="form-popup" id="FormCateg">
                        <form action="addcategory.php" class="form-container">
                           <input type="hidden" name="idRest" value="' . $idR . '">
                           <label for="Nome"><strong>Categoria</strong></label>
                           <input title="Nome piatto" type="text" placeholder="Inserire nome" name="Nome" required>
                           <button type="submit" class="btn">Conferma</button>
                           <button type="button" id="closeCatForm" class="btn cancel">Annulla</button>
                        </form>
                     </div>
                     <div class="menu-block tabbable" tabindex="2" id="divCatAddNew">
                        <p class="text-center align-middle PlusAddNew">&#43;</p>
                     </div>
             </div>';
        }
    echo'</div>';
} else {
    echo '<p align="center"> Non hai ancora aggiunto prodotti al menu.</p>';
    echo '<div id="MenuAddNew" class="col mb40" style="width:100%;">
                     <div class="form-popup" id="FormCateg">
                        <form action="addcategory.php" class="form-container">
                           <input type="hidden" name="idRest" value="' . $idR . '">
                           <label for="Nome"><strong>Categoria</strong></label>
                           <input title="Nome piatto" type="text" placeholder="Inserire nome" name="Nome" required>
                           <button type="submit" class="btn">Conferma</button>
                           <button type="button" id="closeCatForm" class="btn cancel">Annulla</button>
                        </form>
                     </div>
                     <div class="menu-block tabbable" tabindex="2" id="divCatAddNew">
                        <p class="text-center align-middle PlusAddNew">&#43;</p>
                     </div>
             </div>';
}
$conn->close();
?> 