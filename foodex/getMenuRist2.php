<?php
$servername="localhost";
$username ="foodex";
$password ="";
$database = "my_foodex";

$idR = $_SESSION["IDRest"];


// Create connection
$conn = new mysqli($servername, $username, $password, $database);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
$sql = "SELECT * FROM FOOD RIGHT JOIN CATEGORIA ON (Categoria = IdCategoria) WHERE IdRistorante = " . $idR . " ORDER BY Categoria, IdFood";
$result = $conn->query($sql);
$i = 0;
if ($result->num_rows > 0) {
	$oldCategoria = null;
    echo '<div class="row">';
	while($row = $result->fetch_assoc()) {
      $idCategoria = $row["IdCategoria"];
      if($idCategoria != $oldCategoria){
    	$i= $i + 1;
      	if($oldCategoria != null){         
          echo ' 
          	</div>
          </div>';
        }
      	$oldCategoria = $idCategoria;
        
        echo '
			<!--Nuova Categoria-->
        	<div class="col-md-4 mb40">
                    <div class="menu-block">
                        <h3 class="menu-title">' . $row["Nome"] .'</h3>';
      }
      if(isset($row["IdFood"])){
        echo '<div data-id="' . $row["IdFood"] . '" class="menu-content MenuHover">
          <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
              <div class="dish-img"><a href="#"><img src="' . $row["Image"] . '" alt="" class="img-circle "></a></div>
              </div>
              <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                <div class="dish-content">
                  <h5 class="dish-title"><a>' . $row["Name"] .'</a> <input type="button" class="btn btn-mm btn-warning addtocart" value="+" onclick="cart(\'Item'.$row["IdFood"].'_'.$idR. '-'. $row["Price"] . '\')" ' . ($_SESSION["Tipologia"]!="Utente"?"disabled":"") . '></h5>
                  <span class="dish-meta">' . $row["Description"] . '</span>
                  <div class="dish-price">          
                    <p class="dish-price-p">' . $row["Price"] .' €</p>
                  </div>
                </div>
            </div>
          </div>
        </div>';
        }
      }
    
    echo'		</div>
          </div>';
    
    echo'</div>';
} else {
    echo '<p align="center"> Il ristorante non ha ancora aggiunto prodotti al suo menù.</p>';
}
$conn->close();
?> 