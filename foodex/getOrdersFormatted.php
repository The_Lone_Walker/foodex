 <?php
$servername="localhost";
$username ="foodex";
$password ="";
$database = "my_foodex";

$idR = $_POST["IDRest"];

// Create connection
$conn = new mysqli($servername, $username, $password, $database);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
$sql = "SELECT * FROM ORDINE WHERE IdRistorante = " . $idR . " AND Fattorino IS NULL AND Data >= NOW() - INTERVAL 1 DAY ORDER BY IdOrdine ASC";
$result = $conn->query($sql);
if ($result->num_rows > 0){
	while($row = $result->fetch_assoc()) {   	
    	echo '<tr>
                      <th scope="row" id="'. $row['IdOrdine'] .'">'. $row['IdOrdine'] .'</th>
                      <td headers="Data '. $row['IdOrdine'] .'">' . $row['Data'] . '</td>
                      <td headers="Ordine '. $row['IdOrdine'] .'">'; 
      				  $sqlfood = "SELECT * FROM ORDINE_FOOD JOIN FOOD ON ORDINE_FOOD.IdFood = FOOD.IdFood WHERE IdOrdine = " . $row['IdOrdine'];
                      $resultfood = $conn->query($sqlfood);
                      if ($resultfood->num_rows > 0){
                          while($rowfood = $resultfood->fetch_assoc()) {
								echo $rowfood['Qta'] . 'x ' .$rowfood['Name'] . '<br/>';
                          }
                      }
                      echo '</td>
                      <td headers="Totale '. $row['IdOrdine'] .'">' . $row['Totale'] . '</td>
                      <td headers="Indirizzo '. $row['IdOrdine'] .'">' . $row['LuogoConsegna'] . '</td>';
                      $sqluser = "SELECT Indirizzo, Telefono FROM UTENTE WHERE Username = '" . $row['User'] . "'";
                      $resultuser = $conn->query($sqluser);
                      if ($resultuser->num_rows > 0){
                          if($rowuser = $resultuser->fetch_assoc()) {
								echo '<td headers="Telefono '. $row['IdOrdine'] .'">' . $rowuser['Telefono'] . '</td>';
                          }
                      }
                      if(!isset($row['Fattorino'])){
                        echo '<td headers="Fattorino '. $row['IdOrdine'] .'"> 
                        <p class="text-center">Assegna: </p>
                          <select>';
                          $sqlfattorino = "SELECT Nome, Username FROM UTENTE WHERE Ristorante = " . $idR;
                          $resultfattorino = $conn->query($sqlfattorino);
                          if ($resultfattorino->num_rows > 0){
                              while($rowfattorino = $resultfattorino->fetch_assoc()) {
                                  echo '<option value="' . $rowfattorino['Username'] .'">' . $rowfattorino['Nome'] . '</option>';
                              }
                          }
                          echo '</select>
                          <svg xmlns="http://www.w3.org/2000/svg" class="glyph si-glyph-checked ok-fattorino">
                              <title>Assegna</title>
                              <use xlink:href="sprite.svg#si-glyph-checked" />
                            </svg> 
                          </td>';
                        } else{
                        	echo '<td headers="Fattorino '. $row['IdOrdine'] .'">' . $row['Fattorino'] . '<td/>';
                        }
                        echo '</tr>';
    }
} else{
	echo '<p class="text-center">Nessun ordine</p>';
}
$conn->close();
?> 