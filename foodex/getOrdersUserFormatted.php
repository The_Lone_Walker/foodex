 <?php
$servername="localhost";
$username ="foodex";
$password ="";
$database = "my_foodex";

$id = $_POST["IDUser"];

// Create connection
$conn = new mysqli($servername, $username, $password, $database);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
$sql = "SELECT * FROM ORDINE WHERE User = '" . $id . "' AND Data >= NOW() - INTERVAL 1 DAY AND Consegnato = 0";
$result = $conn->query($sql);
if ($result->num_rows > 0){
	while($row = $result->fetch_assoc()) {   	
    	echo '<tr>
                      <th scope="row" id="' . $row['IdOrdine'] .'">'. $row['IdOrdine'] .'</th>
                      <td headers="Data '. $row['IdOrdine'] .'">' . $row['Data'] . '</td>
                      <td headers="Ordine '. $row['IdOrdine'] .'">'; 
        $sqlfood = "SELECT * FROM ORDINE_FOOD JOIN FOOD ON ORDINE_FOOD.IdFood = FOOD.IdFood WHERE IdOrdine = " . $row['IdOrdine'];
        $resultfood = $conn->query($sqlfood);
        if ($resultfood->num_rows > 0){
          while($rowfood = $resultfood->fetch_assoc()) {
            echo $rowfood['Qta'] . 'x ' .$rowfood['Name'] . '<br/>';
          }
        }
        echo '</td>
        <td headers="Totale '. $row['IdOrdine'] .'">' . $row['Totale'] . '</td>';
        if(!isset($row['Fattorino'])){
        	echo '<td headers="Stato '. $row['IdOrdine'] .'" id="stato' . $row['IdOrdine'] . '">Ordine in preparazione</td>';
        } else if($row['Consegnato'] == 0){
        	echo '<td headers="Stato '. $row['IdOrdine'] .'" id="stato' . $row['IdOrdine'] . '">Ordine spedito</td>';
        } else{
        	echo '<td headers="Stato '. $row['IdOrdine'] .'" id="stato' . $row['IdOrdine'] . '">Ordine consegnato</td>';
        }
        echo '</tr>';
    }
} else{
	echo '<p class="text-center">Nessun ordine</p>';
}
$conn->close();
?> 