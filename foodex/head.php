<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta name="theme-color" content="#FFC53A">
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<script src="https://code.jquery.com/jquery-3.3.1.min.js" type="text/javascript"></script>
    <script src="myscripts.js"></script>
    <!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="CommonStyle.css">
    
    <!-- Notification script -->
    <script type="text/javascript">
    	$(document).ready(function(){
        	var id = "<?php echo $_SESSION['username']?>";
            var old = {};
            var json = {};
            function refresh(){
              $('#notifyuser').empty();
              $.post("getOrdersUserFormatted.php" ,{"IDUser" : id } ,function(data) {
               })
               .done(function(data) {
               		$('#notifyuser').append($(data));      
               });
            };
            function updatenotify(){
              $.post("getOrdersUser.php" ,{"IDUser" : id } ,function(data) {
             })
             .done(function(data) {
             	json = $.parseJSON(data);
                $.each( old, function( i, val ) {
                	if(json[i] != val){
                    	
                        $(".notification-bell").css("background-color" , "#E3292A");
                    	$(".notification-bell").css("border" , "solid 1px black"); 
                        $(".notification-bell").css("border-radius" , "10%");
                        
                    	switch(json[i]){
                        	case 0:                 
                            	$("#stato" + i).text("Ordine in preparazione");
                            break;                            
                        	case 1:
                        		alert("L'ordine "+ i + " é stato spedito");     
                            	$("#stato" + i).text("Ordine spedito");                                
                            break;                           
                        	case 2:
                        		alert("L'ordine "+ i + " é stato consegnato");     
                            	$("#stato" + i).text("Ordine consegnato");
                            break;
                        }
                    }
                });
                old = json;
             });
            };
        	  $(".notification-bell").click(function(){         	
              $(".notification-bell").css("background-color" , "");
              $(".notification-bell").css("border" , "");
              
            });
            updatenotify();
            refresh();
            window.setInterval(updatenotify, 5000);
            window.setInterval(refresh, 60000);
        })();
        
    function removeFromCart(id)
    {
	  var ele=id;
      /*
	  var img_src=ele.getElementsByTagName("img")[0].src;
	  var name=document.getElementById(id+"_name").value;
	  var price=document.getElementById(id+"_price").value;	
      */
	  $.ajax({
        type:'post',
        url:'store_items.php',
        data:{
          item_idRem:ele
        }
      });	
    }
    
    function updateDeliveryAddress(id)
    {
	  var ele = document.getElementById(id).value;
      /*
	  var img_src=ele.getElementsByTagName("img")[0].src;
	  var name=document.getElementById(id+"_name").value;
	  var price=document.getElementById(id+"_price").value;	
      */
	  $.ajax({
        type:'post',
        url:'store_items.php',
        data:{
          item_consegna:ele
        }
      });	
    }
    </script> 
    
    <title>FoodEx</title>
  </head>
  <body>
	<header class="yellow">
		<nav class="navbar navbar-expand-lg navbar-light">
		  <a class="navbar-brand" class="img-responsive" href="/"><img src="images/logo.png" alt='logo' class = "menuLogo"></a>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		  <span class="navbar-toggler-icon"></span>
		  </button>
		  <div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
			</ul>
			<ul class="navbar-nav my-2 my-lg-0">		
			
			  <li class="nav-item menu-option">
				<a class="nav-link" href="restaurantsList.php"><span>Ristoranti</span></a>
			  </li>
			  <li class="nav-item menu-option">
				<a class="nav-link" href="Shopping_Cart.php"><span>Carrello</span></a>
			  </li>             
             <li class="nav-item menu-option">
               	<a class="nav-link" href="Profile.php"><span>Profilo</span></a>
             </li>
			 <li class="nav-item order-1 menu-option notification-bell" data-toggle="modal" data-target="#NotificheModal">
               <svg xmlns="http://www.w3.org/2000/svg" class="glyph si-glyph-bell">
                 <title>Notifiche</title>
                 <use xlink:href="sprite.svg#si-glyph-bell" />
               </svg> 
             </li>
             <li class="nav-item menu-option">
               	<a class="nav-link" href="logout.php"><span>Logout</span></a>
             </li>
            </ul>
		  </div>
		</nav>
	</header> 
    
    <!-- Modal -->
  <div id="NotificheModal" class="modal fade" role="dialog">
     <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
           <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">×</button>
              <h4 class="modal-title" style="position:fixed;">Ordini:</h4>
           </div>
           <div class="modal-body table-responsive">
           		<table class="table table-striped">
                  <thead>
                    <tr>
                      <th scope="col" id="ID">#ID</th>
                      <th scope="col" id="Data">Data</th>
                      <th scope="col" id="Ordine">Ordine</th>
                      <th scope="col" id="Totale">Totale</th>
                      <th scope="col" id="Stato">Stato</th>
                    </tr>
                  </thead>
                  <tbody id="notifyuser">                              
                  </tbody>
                </table>
           </div>
        </div>
     </div>
  </div> 