<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta name="theme-color" content="#FFC53A">
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<script src="jquery-1.11.3.min.js" type="text/javascript"></script>
    <!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="prova.css">
    <link rel="stylesheet" type="text/css" href="CommonStyle.css">
    
    <title>FoodEx</title>
  </head>
  <body>
	<header class="yellow">
		<nav class="navbar navbar-expand-lg navbar-light">
		  <a class="navbar-brand" href="/"><img src="images/logo.png" alt='logo' class = "menuLogo"></a>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		  <span class="navbar-toggler-icon"></span>
		  </button>
		  <div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
			</ul>
			<ul class="navbar-nav my-2 my-lg-0">
			 <li class="dropdown order-1 "> <!--tolto menu-option dai tag -->
                    <button type="button" id="dropdownMenu1" data-toggle="dropdown" class="btn btn-menu yellow dropdown-toggle">Login <span class="caret"></span></button>
                    <ul class="dropdown-menu dropdown-menu-right mt-2">
                       <li class="px-3 py-2" style = "padding:0% !important;">
                           <form class="form mainForm" role="form" method="get" action="login.php">
                                <div class="form-group">
                                	<label for="emailInput" style="display:none;">Username:</label>
                                    <input id="emailInput" name="user" placeholder="Username" class="form-control form-control-sm" type="text" required="">
                                </div>
                                <div class="form-group">
                                	<label for="passwordInput" style="display:none;">Password:</label>
                                    <input id="passwordInput" name="password" placeholder="Password" class="form-control form-control-sm" type="password" required="">
                                </div>
                                <div class="form-group">
                                    <button id="btn-login" type="submit" class="btn btn-primary btn-block">Login</button>
                                </div>
							    <div class="form-group text-center">
                                    <small><a href="registration.php" class="grey-text">Non hai un account? Registrati!</a></small>
                                </div>
                            </form>
                        </li>
                    </ul>
                </li>
		
             </ul>
		  </div>
		</nav>
	</header> 