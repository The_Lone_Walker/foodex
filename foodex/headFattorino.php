<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta name="theme-color" content="#FFC53A">
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<script src="https://code.jquery.com/jquery-3.3.1.min.js" type="text/javascript"></script>
    <script src="myscripts.js"></script>
    <!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="CommonStyle.css">
    
    <!-- Notification script -->
    <script type="text/javascript">
    	$(document).ready(function(){
        	var id = "<?php echo $_SESSION['username']?>";
            var last = -1;
            function refresh(){
              $.post("getOrdersFatt.php" ,{"IDFatt" : id } ,function(data) {
               })
               .done(function(data) {
				  if(data != null && data > last){  
                  	$("#ordinifatt").css("background-color" , "#E3292A");
                 	$("#ordinifatt").css("border" , "solid 1px black");
                    $("#ordinifatt").css("border-radius" , "10%");
                    $("#ordinifatt").css("padding" , "8px");
                    
            		last = data;
                    updateTable();
                    alert("Ci sono nuovi ordini da consegnare");
                  }
               });
            };  
            
            function updateTable(){
              $.post("getAllOrdersForWorkers.php" ,{"ID" : id} ,function(data) {
               })
               .done(function(data) {
                  $("#TabFattOrdini").empty();
				  $("#TabFattOrdini").append(data);
               });
            };  
            refresh();         
            window.setInterval(refresh, 5000);
          });
    </script>
    <title>FoodEx</title>
  </head>
  <body>
	<header class="yellow">
		<nav class="navbar navbar-expand-lg navbar-light">
		  <a class="navbar-brand" href="/"><img src="images/logo.png" alt='logo' class = "menuLogo"></a>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		  <span class="navbar-toggler-icon"></span>
		  </button>
		  <div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
			</ul>
			<ul class="navbar-nav my-2 my-lg-0">		
			
			  <li class="nav-item menu-option">
				<a class="nav-link" href="paginaFattorino.php" id="ordinifatt"><span>Ordini</span></a>
			  </li>             
             <li class="nav-item menu-option">
               	<a class="nav-link" href="Profile.php"><span>Profilo</span></a>
             </li>
             <li class="nav-item menu-option">
               	<a class="nav-link" href="logout.php"><span>Logout</span></a>
             </li>
            </ul>
		  </div>
		</nav>
	</header> 