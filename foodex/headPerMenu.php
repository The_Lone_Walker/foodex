<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta name="theme-color" content="#FFC53A">
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<script src="jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="myscripts.js"></script>
    <!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="CommonStyle.css">
    
    
    
     <script type="text/javascript">

    function cart(id)
    {
	  var ele=id.substring(0,id.indexOf('_'));
      var resto=id.substring(id.indexOf('_')+1);
      var idRest = resto.substring(0,resto.indexOf('-'));
      var iprice = resto.substring(resto.indexOf('-')+1);
	  $.ajax({
        type:'post',
        url:'store_items.php',
        data:{
          item_id:ele,
          rest_id:idRest,
          price:iprice
        }
      });	
    }
	
</script>
    
    
    
    
    <title>FoodEx</title>
  </head>
  <body>
	<header class="yellow">
		<nav class="navbar navbar-expand-lg navbar-light">
		  <a class="navbar-brand" href="/"><img src="images/logo.png" alt='logo' class = "menuLogo"></a>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		  <span class="navbar-toggler-icon"></span>
		  </button>
		  <div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
			</ul>
			<ul class="navbar-nav my-2 my-lg-0">		
			
			  <li class="nav-item menu-option">
				<a class="nav-link" href="restaurantsList.php"><span>Ristoranti</span></a>
			  </li>
			  <li class="nav-item menu-option">
				<a class="nav-link" href="Shopping_Cart.php"><span>Carrello</span></a>
			  </li>             
             <li class="nav-item menu-option">
               	<a class="nav-link" href="Profile.php"><span>Profilo</span></a>
             </li>
			 <li class="nav-item order-1 menu-option notification-bell" data-toggle="modal" data-target="#NotificheModal">
               <svg xmlns="http://www.w3.org/2000/svg" class="glyph si-glyph-bell">
                 <title>Notifiche</title>
                 <use xlink:href="sprite.svg#si-glyph-bell" />
               </svg> 
             </li>
             <li class="nav-item menu-option">
               	<a class="nav-link" href="logout.php"><span>Logout</span></a>
             </li>
            </ul>
		  </div>
		</nav>
	</header> 
    
    <!-- Modal -->
  <div id="NotificheModal" class="modal fade" role="dialog">
     <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
           <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">×</button>
              <h4 class="modal-title" style="position:fixed;">Ordini:</h4>
           </div>
           <div class="modal-body table-responsive">
           		<table class="table table-striped">
                  <thead>
                    <tr>
                      <th scope="col">#ID</th>
                      <th scope="col">Data</th>
                      <th scope="col">Ordine</th>
                      <th scope="col">Totale</th>
                      <th scope="col">Stato</th>
                    </tr>
                  </thead>
                  <tbody id="notifytable">                              
                  </tbody>
                </table>
           </div>
        </div>
     </div>
  </div>