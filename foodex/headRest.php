<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta name="theme-color" content="#FFC53A">
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<script src="https://code.jquery.com/jquery-3.3.1.min.js" type="text/javascript"></script>
    <!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="CommonStyle.css">
    <!-- Notification script -->
    <script type="text/javascript">
    	$(document).ready(function(){
        var id = <?php echo $_SESSION["IDRest"]?>;
       	var last = -1;
        var i = 0;
        refresh();
        function refresh(){
        	$('#notifytable').empty();
             $.post("getOrdersFormatted.php" ,{"IDRest" : id } ,function(data) {
             })
             .done(function(data) {
               $('#notifytable').append($(data));
               $(".ok-fattorino").click(function(){
               
                        	
              $(".notification-bell").css("background-color" , "");
              $(".notification-bell").css("border" , "");
               
                    var dad = $(this).parent().parent();
                    $.post("assignCourier.php" ,{"IdOrder" : dad.find("th").text() ,"Fatt" : dad.find("td:last select").val()} ,function(data) {
                    })
                    .done( function(){
                      alert("Fattorino Assegnato");
                      refresh();
                    });
               });            
             });
             
        };
        function updatenotify(){
                $.post("getOrders.php" ,{"IDRest" : id } ,function(data) {
                    if( data != 0 ){                  		
                    	$(".notification-bell").css("background-color" , "#E3292A");
                    	$(".notification-bell").css("border" , "solid 1px black");
                        $(".notification-bell").css("border-radius" , "10%");
                        
                        $.post("getLastOrder.php" ,{"IDRest" : id } ,function(sqlLast) {                     	
                            if(last != sqlLast){
                            	i=0;
                            	refresh();
                            	last = sqlLast;
                                alert("Hai dei nuovi ordini");
                            }
                            if(i>=6){
                            	i=0;
                          		refresh();
                            }
                            i++;
                        }); 
                    }                   
                });
            };
            updatenotify();
            window.setInterval(updatenotify, 10000);
        })();
    </script>
    
    <title>FoodEx</title>
  </head>
  <body>
	<header class="yellow">
		<nav class="navbar navbar-expand-lg navbar-light">
		  <a class="navbar-brand" href="/"><img src="images/logo.png" alt='logo' class = "menuLogo"></a>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		  <span class="navbar-toggler-icon"></span>
		  </button>
		  <div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
			</ul>
			<ul class="navbar-nav my-2 my-lg-0">
                                   
             <li class="nav-item menu-option">
               	<a class="nav-link" href="RistMenu.php"><span>Menù Digitale</span></a>
             </li>
             
             
             <li class="nav-item menu-option">
               	<a class="nav-link" href="gestioneFattorini.php"><span>Fattorini</span></a>
             </li>
             
              <li class="nav-item menu-option">
               	<a class="nav-link" href="registraFattorino.php"><span>Assumi Fattorino</span></a>
             </li>
                        
             
             <li class="nav-item menu-option">
               	<a class="nav-link" href="Profile.php"><span>Profilo</span></a>
             </li>
			 <li class="nav-item order-1 menu-option notification-bell" data-toggle="modal" data-target="#NotificheModal">
               <svg xmlns="http://www.w3.org/2000/svg" class="glyph si-glyph-bell">
                 <title>Notifiche</title>
                 <use xlink:href="sprite.svg#si-glyph-bell" />
               </svg> 
             </li>
             <li class="nav-item menu-option">
               	<a class="nav-link" href="logout.php"><span>Logout</span></a>
             </li>
            </ul>
		  </div>
		</nav>
	</header>
    

  <!-- Modal -->
  <div id="NotificheModal" class="modal fade" role="dialog">
     <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
           <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">×</button>
 				<h4 class="modal-title" style="position:fixed;">Ordini:</h4>
           </div>
           <div class="modal-body table-responsive">
           		<table class="table table-striped">
                  <thead>
                    <tr>
                      <th scope="col" id="ID">#ID</th>
                      <th scope="col" id="Data">Data</th>
                      <th scope="col" id="Ordine">Ordine</th>
                      <th scope="col" id="Totale">Totale</th>
                      <th scope="col" id="Indirizzo">Indirizzo</th>
                      <th scope="col" id="Telefono">Telefono</th>
                      <th scope="col" id="Fattorino">Fattorino</th>
                    </tr>
                  </thead>
                  <tbody id="notifytable">                              
                  </tbody>
                </table>
           </div>
        </div>
     </div>
  </div>

