<?php

session_start();

$servername="localhost";
$username ="foodex";
$password ="";
$database = "my_foodex";

if( isset($_GET['user']) && isset($_GET['password'])){
    $con = new mysqli($servername, $username, $password, $database);
    $stmt = $con->prepare("SELECT * FROM UTENTE WHERE Username = ?");
    $stmt->bind_param('s', $_GET['user']);
    $stmt->execute();
    $result = $stmt->get_result();
    $user = $result->fetch_object();

    // Verify user password and set $_SESSION
    if ( password_verify( $_GET['password'], $user->Password ) ) {
      $_SESSION['email'] = $user->Email;
      $_SESSION['nome'] = $user->Nome;
      $_SESSION['username'] = $user->Username;
      $_SESSION['indirizzo'] = $user->Indirizzo;
      $_SESSION['telefono'] = $user->Telefono;
      $_SESSION['Image'] = $user->Image;
      if($user->TipoAccount == 0){
       $_SESSION['Tipologia'] = "Utente";
       $_SESSION['Carrello'] = array();
       $_SESSION['Quantity'] = array();
       $_SESSION["RistorantiDiAppartenenza"] = array();
       $_SESSION['ShoppingPrice'] = array();
       $_SESSION['LuogoConsegna'] = "IngressoPrimoPiano"; 
      } else if($user->TipoAccount == 1) {
     	 $_SESSION['Tipologia'] = "Admin";
      } else {
      	 $_SESSION['Tipologia'] = "Fattorino";
      }
 	  header("location: index.php");
    } else{
      $stmt = $con->prepare("SELECT * FROM RESTAURANT WHERE Username = ?");
      $stmt->bind_param('s', $_GET['user']);
      $stmt->execute();
      $result = $stmt->get_result();
      $user = $result->fetch_object();

      // Verify user password and set $_SESSION
      if ( password_verify( $_GET['password'], $user->Password )){
      	$_SESSION['IDRest'] = $user->IdRestaurant;
      	$_SESSION['email'] = $user->Email;
        $_SESSION['nome'] = $user->Name;
        $_SESSION['username'] = $user->Username;
        $_SESSION['Ristorante'] = $user->Name;
        $_SESSION['indirizzo'] = $user->Address;
        $_SESSION['telefono'] = $user->Telefono;
        $_SESSION['Image'] = $user->Image;
    	$_SESSION['Tipologia'] = "Ristorante";
        header("location: index.php");
      }
      else{
 		header("location:index.php?msg=failed");
      }
    }
}
?>