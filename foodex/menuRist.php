 <?php 
 	session_start();
    if(isset($_SESSION['nome'])){
    	if($_SESSION['Tipologia'] == "Ristorante"){
        	include 'headRest.php';
        } else if($_SESSION['Tipologia'] == "Fattorino"){
        	include 'headFattorino.php';
        } else if($_SESSION['Tipologia'] == "Admin"){
        	include 'headAdmin.php';
        } else {
			include 'headPerMenu.php';
    	}
    }
    else{
    	include 'head2.php';
    }
    $_SESSION['IDRest'] = $_GET['idrist'];
?>

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
$(document).ready(function() {
	$('.addtocart').click(function() {
  		alert("Prodotto Aggiunto al Carrello!");
	});
});
</script>

    <!-- menu -->
    <div class="content FoodMenu">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                    <div class="page-section">                     
                      <h1 class="page-title">Menù</h1>
                    </div>
                </div>
            </div>
                <!-- GET MENU RIST  -->
                        <?php 
                       	include "getMenuRist2.php"; 
                        ?>             
                <!-- . GET MENU RIST -->
    	</div>
	</div>
    <!-- /.menu -->
   
   
<?php 
	include 'footer.php';
?>