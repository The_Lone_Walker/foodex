<?php 
	session_start();
    if(isset($_SESSION['nome'])){
    	if($_SESSION['Tipologia'] == "Ristorante"){
        	include 'headRest.php';
        } else if($_SESSION['Tipologia'] == "Fattorino"){
        	include 'headFattorino.php';
        } else if($_SESSION['Tipologia'] == "Admin"){
        	include 'headAdmin.php';
        } else {
			include 'head.php';
    	}
    }
    else{
    	include 'head2.php';
    }
    if(isset( $_SESSION['nome']) and $_SESSION['Tipologia'] == "Fattorino")
    {
?>

<h2 style ="margin-top:50px;">I tuoi ordini</h2>
<table id="example" class="fattorinoTable table table-striped table-bordered dt-responsive nowrap" style="width:100%;margin-top:0.5%;">
        <thead>
            <tr>
                <th class ="center" scope="col" id="ID">IDOrdine</th>
                <th class ="center" scope="col" id="Totale">Totale</th>
                <th class ="center" scope="col" id="Data">Data</th>
                <th class ="center" scope="col" id="Username">Username</th>
                <th class ="center" scope="col" id="Nome">Nome</th>
                <th class ="center" scope="col" id="Indirizzo">LuogoScelto</th>
                <th class ="center" scope="col" id="Telefono">Telefono</th>
                <th class ="center" scope="col" id="Stato">Stato</th>
            </tr>
        </thead>
        <tbody id="TabFattOrdini">
       					<?php
					require 'getAllOrdersForWorkers.php';
					?>
        </tbody>
    </table>
    
<?php 
	} else{   
?>
	<div class="container bootstrap snippet">
    <div class="row">
  		<div class="col-sm-10 grey-text middle-error"><h1> Non possiedi i permessi per accedere alla seguente pagina! </h1></div> 
    </div>
    </div>  
<?php
	}
	include 'footer.php';
?>