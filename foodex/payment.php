<?php 
	session_start();
    if(isset($_SESSION['nome'])){
    	if($_SESSION['Tipologia'] == "Ristorante"){
        	include 'headRest.php';
        } else if($_SESSION['Tipologia'] == "Fattorino"){
        	include 'headFattorino.php';
        } else if($_SESSION['Tipologia'] == "Admin"){
        	include 'headAdmin.php';
        } else {
			include 'head.php';
    	}
    }
    else{
    	include 'head2.php';
    }

    $servername="localhost";
	$username ="foodex";
	$password ="";
	$database = "my_foodex";
    
    $prodotti = $_SESSION['Carrello'];
    $ristoranti = $_SESSION['RistorantiDiAppartenenza'];
    $quantity = $_SESSION['Quantity'];
    $costi = $_SESSION['ShoppingPrice'];
    $distinctRest = array();
    
    //inserisco tutti i ristoranti distinti nell'array
    for($i = 0; $i < count($ristoranti); $i++){
    	if(!in_array($ristoranti[$i], $distinctRest) && $ristoranti[$i] != -1){
          array_push($distinctRest,$ristoranti[$i]);
        }
    }
    
   // echo count($distinctRest);
    for($i = 0; $i < count($distinctRest); $i++){
   		$tempProd = array();
   		$tempQts = array();
        $tempTotal = 0;
    	for($k = 0; $k < count($prodotti); $k++){
        	if( $ristoranti[$k]!= -1 && $ristoranti[$k] == $distinctRest[$i]){
            	array_push($tempProd,$prodotti[$k]);
                array_push($tempQts,$quantity[$k]);
                $tempTotal += $costi[$k] * $quantity[$k];
            }
        }
                                                       
    $sql = ("INSERT INTO ORDINE (IdRistorante, User, Totale, Data, LuogoConsegna) VALUES(?, ?, ?, ?, ?)");
    $con = new mysqli($servername, $username, $password, $database);
    
    if($stmt = $con->prepare($sql)) { // assuming $mysqli is the connection
    
    $IdRistorante = $distinctRest[$i];
    $username = $_SESSION['username'];
    $totale = $tempTotal;
    $data = date("Y-m-d");
    $luogoConsegna = $_SESSION['LuogoConsegna'];

    $stmt->bind_param('isdss', $IdRistorante, $username, $totale, $data, $luogoConsegna);
    if($stmt->execute()){
    	
        //esecuzione riuscita
        $last_id = mysqli_insert_id($con);
       	$sql = ("INSERT INTO ORDINE_FOOD (IdOrdine, IdFood, Qta) VALUES(?, ?, ?)");
        for($m = 0; $m < count($tempProd); $m++){
        	if($stmt = $con->prepare($sql)) {
            $xd=str_replace("Item","",$tempProd[$m]);
            $quaaa=$tempQts[$m];
            	 $stmt->bind_param('iii', $last_id, $xd, $quaaa);
                 if($stmt->execute()){
                 
                 } else {
                 	echo $stmt->error;
                 }
            } else {
                $error = $con->errno . ' ' . $con->error;
   				 echo $error;
            }
        }
    } else {
        echo $stmt->error;
    }
	} else {
    $error = $con->errno . ' ' . $con->error;
    echo $error;
	}     
    }
    
    //svuoto le sessioni
    $_SESSION['Carrello']=array();
    $_SESSION['Quantity']=array();
    $_SESSION["RistorantiDiAppartenenza"]=array();
    $_SESSION["ShoppingPrice"]=array();
    $_SESSION['LuogoConsegna'] = "Blocco A";
     
?>
	<div class="container bootstrap snippet">
    <div class="row" align="center">
  		<div class="col-sm-10 grey-text middle-error" align="center">
        <h1> Pagamento Effettuato! <br> Clicca <a href="index.php">qui</a> per tornare nella Home Page! </h1> <hr>
      <!-- <h1>Clicca qui per per pagare! </h1>  -->
      <!-- <script src="https://www.paypal.com/sdk/js?client-id=sb"></script>
<script>paypal.Buttons().render('body');</script> -->
        <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
<input type="hidden" name="cmd" value="_s-xclick">
<input type="hidden" name="hosted_button_id" value="D3FZG3PLKF9FA">
<input type="image" src="https://www.paypalobjects.com/it_IT/IT/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal è il metodo rapido e sicuro per pagare e farsi pagare online.">
<img alt="" border="0" src="https://www.paypalobjects.com/it_IT/i/scr/pixel.gif" width="1" height="1">
</form>       
        </div> 
    </div>
    </div>  
<?php
	include 'footer.php';
?>