<?php
session_start();

$servername="localhost";
$username ="foodex";
$password ="";
$database = "my_foodex";

$ID = $_POST['idCat'];
if(isset($ID)){
  $con = new mysqli($servername, $username, $password, $database);

  $stmt = $con->prepare("DELETE FROM CATEGORIA WHERE IdCategoria = ?");
  $stmt->bind_param("i", $ID);
  $result = $stmt->execute();
  if($result){
  	echo "Categoria eliminata";
  } else {
  	echo "Errore eliminazione categoria";
  }

  $stmt->close();
  $con->close();
}
?>