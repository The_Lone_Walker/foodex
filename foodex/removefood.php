<?php
session_start();

$servername="localhost";
$username ="foodex";
$password ="";
$database = "my_foodex";

$ID = $_POST['IdFood'];
if(isset($ID)){
  $con = new mysqli($servername, $username, $password, $database);

  $stmt = $con->prepare("DELETE FROM FOOD WHERE IdFood = ?");
  $stmt->bind_param("i", $ID);
  $result = $stmt->execute();
  if($result){
  	echo "Piatto eliminato";
  } else {
  	die(header("Errore eliminazione piatto"));
  }

  $stmt->close();
  $con->close();
}
?>