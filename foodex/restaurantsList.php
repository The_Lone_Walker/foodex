<?php 
	session_start();
    if(isset($_SESSION['nome'])){
    	if($_SESSION['Tipologia'] == "Ristorante"){
        	include 'headRest.php';
        } else if($_SESSION['Tipologia'] == "Fattorino"){
        	include 'headFattorino.php';
        } else if($_SESSION['Tipologia'] == "Admin"){
        	include 'headAdmin.php';
        } else {
			include 'head.php';
    	}
    }
    else{
    	include 'head2.php';
    }

$servername="localhost";
$username ="foodex";
$password ="";
$database = "my_foodex";
	
    $con = new mysqli($servername, $username, $password, $database);
    $sql = ("SELECT * FROM RESTAURANT");
    
    if($stmt = $con->prepare($sql)){
    	if($stmt->execute()){
       		 $result = $stmt->get_result();
       		 $row = mysqli_fetch_all($result,MYSQLI_ASSOC);
             $totRest = count($row);
        }
    }
?>
<h2 style="margin-top: 2%;"> I Nostri Ristoranti </h2>
<!--<button type="button" class="btn btn-info float-right botyellow">Filter</button>-->
<!-- INIZIO GALLERIAAAA -->
<div class="container-fluid bg1" style="background-color:white!important">
<?php
for($i = 0; $i < $totRest; $i++){
	if($i % 3 == 0){
		echo '<div class="row topMargin">';  
     }
?>
  <!-- Grid column -->
  <div class="col-lg-4 col-md-12 mb-4">

    <!--Modal: Name-->
    <div class="modal fade" id="modal<?php echo $i;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel<?php echo $i;?>" aria-hidden="true">
      <div class="modal-dialog modal-lg" id="myModalLabel<?php echo $i;?>" role="document">

        <!--Content-->
        <div class="modal-content">

          <!--Body-->
          <div class="modal-body mb-0 p-0">

            <div class="embed-responsive embed-responsive-16by9 z-depth-1-half">
              <img class="img-fluid" alt="<?php echo $row[$i]["Image"];?>" src="restaurantsImages/<?php echo $row[$i]["Image"]; ?>.jpg">
            </div>

          </div>

          <!--Footer-->
          <div class="modal-footer justify-content-center">
            <div class="mr-4"><?php echo '<p style="margin-bottom:-10px; text-align: center;">'.$row[$i]["Name"].'</p>';?><br>
            <?php echo "<strong>Indirizzo:</strong> ".$row[$i]["Address"];?><br><?php echo "<strong>Email:</strong> "
            .$row[$i]["Email"];?><br><?php echo "<strong>Telefono:</strong> ".$row[$i]["Telefono"];?></div>                         
            <a class="btn btn-outline-primary btn-rounded btn-md ml-4" role="button" href="menuRist.php?idrist=<?php echo $row[$i]["IdRestaurant"] . '"'; ?> >Mostra</a>
          </div>

        </div>
        <!--/.Content-->

      </div>
    </div>
    <!--Modal: Name-->

    <a><img class="img-fluid z-depth-1" src="restaurantsImages/<?php echo $row[$i]["Image"]; ?>.jpg" alt="<?php echo $row[$i]["Image"];?>"
        data-toggle="modal" data-target="#modal<?php echo $i;?>"></a>
    <p class = "description"> <?php echo '<p align="center" style="color: black; font-weight: bold; margin-top:-15px">'.$row[$i]["Name"].'</p>'; ?></p>
  </div>
<?php
  if($i % 3 == 2){
      echo "</div>";
  }
}
?>
</div>
<?php 
	include 'footer.php';
?>