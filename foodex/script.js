$(document).ready(function(){
	initialise();
    
    $(".tabbable").on("click enter",function(){
    	this.click();
    })
    .on('keypress', function(e) {
        if(e.which === 13) {
            $(this).trigger( 'enter' );
        }
    });
    
	function initialise()
    {
    	//remove img function	
         $(document).on( 'click','.remove-img', function(e) {
         	if (e.target !== this) {
            	return;
            }
         	var id = $(this).parent().parent().parent().attr("data-id");
            var rem = $(this);
            $.post("removedishimage.php", {'IdFood' : id}, function(data) {
            })
            .done(function() {
            	alert("Immagine rimossa");
                $("#img" + id).attr('src', "http://via.placeholder.com/70x70");
            	rem.remove();
            });
         });
    	var idfood;
        var noimage;
    	//change image click    	
         $(document).on('click','.rist-img-editable', function() {
         	idfood = $(this).attr("data-id");
            if( $("#img" + idfood).attr('src') == 'http://via.placeholder.com/70x70' ){
            	noimage = true;
            } else {
            	noimage = false;
            }
           	$("#my_file").click();            
         });
        $("#my_file").change(function (){
        	var file_data = $('#my_file').prop('files')[0];   
            var form_data = new FormData();                  
            form_data.append('IdFood', idfood);
            form_data.append('userfile', file_data);
            $.ajax({
              type:'POST',
              url:'uploadmenuimg.php',
              dataType: 'json',
              processData: false,
              contentType: false,
              data: form_data,
              success: function(response){
                if(response.b != "0"){
                  alert("Immagine caricata");
                  $("#img" + idfood).attr('src', response.b + '?'+Math.random());
                  if( noimage ){
                  	$("<p class=\"remove-img\">Rimuovi</p>").insertAfter($("#img" + idfood).parent().parent());                   
                   }
                } else {
                	alert("Errore");
                }
              }
            });
        });
        // create event here that needs re-initialising
        //Add category div click
        $(document).on( 'click','#divCatAddNew', function(e) {
        	if (e.target !== this){
              return;
            }
            isChanging = 1;
            $("#divCatAddNew").css("display", "none");
            $("#FormCateg").css("display", "block");
            $("#MenuAddNew").css("background-color", "white");
        });

        //Undo add category div click
        $(document).on( 'click','#closeCatForm', function(e) {
        	if (e.target !== this){
              return;
            } 
            isChanging = 0;
            $("#divCatAddNew").css("display", "block");
            $("#FormCateg").css("display", "none");
            $("#MenuAddNew").css("background-color", "#88898c");
        });

        //edit menu title
        $(document).on( 'click','div.edit_menutitle', function(e) {
        	if (e.target !== this){
              return;
            }	
            var dad = $(this).parent();
            $(this).hide();
            dad.find('.menu-title').hide();
            dad.find('input.edit-input-title').show().focus();
            dad.find('.edit-title-accept').css("display", "inline-block");
            dad.find('.edit-undo-title').css("display", "inline-block");
        });

        //undo title changes
        $(document).on( 'click','div.edit-undo-title', function(e) {
        	if (e.target !== this){
              return;
            }
            var dad = $(this).parent();
            $(this).hide();
            dad.find('.menu-title').css("display", "inline-block");
            dad.find('.edit_menutitle').css("display", "inline-block");
            dad.find('input.edit-input-title').hide();
            dad.find('.edit-title-accept').hide();
        });

        //change dish info
        $(document).on( 'click','h5.dish-title-rist', function() {
            var dad = $(this).parent().parent();
            $(this).hide();
            dad.find('.MenuRemove').hide();
            dad.find('.dish-meta').hide();
            dad.find('.dish-price-p').hide();     
            dad.find('.dish-edit-text').show();
            dad.find('input.edit-menuelement-title').show().focus();
            dad.find('input.edit-menuelement-description').show();
            dad.find('input.edit-menuelement-price').show();
            dad.find('.edit-accept-menuelement').css("display", "inline-block");
            dad.find('.edit-undo-menuelement').css("display", "inline-block");
        });   

        //undo menu dish changes
        $(document).on( 'click','div.edit-undo-menuelement', function() {
            var dad = $(this).parent();
            $(this).hide();

            dad.find('.MenuRemove').show();
            dad.find('.dish-title').show();
            dad.find('.dish-meta').show();
            dad.find('.dish-price-p').show();     
            dad.find('.dish-edit-text').hide();
            dad.find('input.edit-menuelement-title').hide();
            dad.find('input.edit-menuelement-description').hide();
            dad.find('input.edit-menuelement-price').hide();
            dad.find('.edit-accept-menuelement').hide();
        });

        //remove dish
        $(document).on( 'click','.MenuRemove', function() {
        	var remove = $(this).parent().parent().parent();
            // issue an AJAX request with HTTP post to your server side page. 
            //Here I used an aspx page in which the update login is written      
            $.post("removefood.php", {'IdFood' : $(this).attr("data-id")}, function(data) {
                alert( data );
            })
            .done(function() {
            	remove.remove();
            });
            return false;
        });

        //remove Category
        $(document).on( 'click','.CategoryRemove', function(e) {
        	if (e.target !== this){
              return;
            }
            // issue an AJAX request with HTTP post to your server side page. 
            //Here I used an aspx page in which the update login is written      
            $.post("removeCategory.php", {'idCat' : $(this).attr("data-id")}, function(data) {
                alert( data );
            })
            .done(function() {
                 location.reload();
            });
            return false;
        });

        //update Category title
        $(document).on( 'click','.edit-title-accept', function(e) {
        	if (e.target !== this){
              return;
            }
            var dad = $(this).parent();
            var newTitle = $(this).parent().find('input[type="text"]').val();
            // issue an AJAX request with HTTP post to your server side page. 
            //Here I used an aspx page in which the update login is written      
            $.post("updatecategory.php", {'idCat' : $(this).attr("data-idCat"),'Nome' : $(this).parent().find('input[type="text"]').val()}, function(data) {
                alert( data );
            })
            .done(function() {
              dad.find('.menu-title').text(newTitle);
              dad.find('.menu-title').css("display", "inline-block");
              dad.find('.edit_hover_class').css("display", "inline-block");
              dad.find('input[type="text"]').hide();
              dad.find('.edit-title-accept').hide();
              dad.find('.edit-undo-title').hide();
            });
            // to prevent the default action
            return false;
        });

        //update Dish element
        $(document).on( 'click','.edit-accept-menuelement', function() {
            var dad = $(this).parent();
            var newTitle = $(this).parent().find('input.edit-menuelement-title').val();
            var newDesc = $(this).parent().find('input.edit-menuelement-description').val();
            var newPrice = parseFloat($(this).parent().find('input.edit-menuelement-price').val());
            // issue an AJAX request with HTTP post to your server side page. 
            //Here I used an aspx page in which the update login is written      
            $.post("updatedish.php", {'idDish' : $(this).attr("data-idDish"),'Title' : $(this).parent().find('input.edit-menuelement-title').val(),'Description' : $(this).parent().find('input.edit-menuelement-description').val(),'Price' : $(this).parent().find('input.edit-menuelement-price').val()}, function(data) {
                alert( data );
            })
            .done(function() {
              dad.find('.MenuRemove').show();
              dad.find('.dish-title a').text(newTitle);
              dad.find('.dish-title').show();
              dad.find('.dish-meta').text(newDesc);
              dad.find('.dish-meta').show();
              dad.find('.dish-price-p').text(newPrice.toFixed(2) + " €");
              dad.find('.dish-price-p').show();     
              dad.find('.dish-edit-text').hide();
              dad.find('input.edit-menuelement-title').hide();
              dad.find('input.edit-menuelement-description').hide();
              dad.find('input.edit-menuelement-price').hide();
              dad.find('.edit-accept-menuelement').hide();
              dad.find('.edit-undo-menuelement').hide();
            })
            .fail(function() {
                alert( "Errore");
            });
            // to prevent the default action
            return false;
        });    
    }
	$(".slider-image > img").not(".slider-image > img:first-child").hide();
	setTimeout(showSlides, 2000);
    var isChanging = 0;

    function showSlides() {
        if($(".slider-image > img").length > 1){		
			var current = $(".slider-image > img:visible");
			if(current.is($(".slider-image > img:last-child"))){
				current.hide();
				$(".slider-image > img:first-child").show();
			}
			current.hide().next().show();
       		setTimeout(showSlides, 2000);
		}
    };
      //setting hover effect here since there were bugs using css
    $("#MenuAddNew").mouseenter(function() {
    	if(isChanging != 1){
        	$(this).css("background", "#b1b2b4");
        }
    }).mouseleave(function() {
    	if(isChanging != 1){
        	 $(this).css("background", "#88898c");
        }
    });      
	
   	var add_idcat;
    var add_parent;
    //Add new dish pressed
    $('.type-1').on( "click", function() {   
    	add_idcat = $(this).attr("data-idcat");
        add_parent = $(this).parent().parent();
    });

	$("#myModal").on("hidden.bs.modal", function () {
      add_parent = null;
      add_idcat = null;
      $('form#reused_form').show();
      $('#success_message').hide();
      $('#error_message').hide();
      $('form#reused_form').find("input, textarea").val("");
    });

	function resetsubmit() {
        $("#myModal").modal('hide');
    	add_parent = null;
     	add_idcat = null;
      	$('form#reused_form').show();
      	$('#success_message').hide();
      	$('#error_message').hide();
      	$('form#reused_form').find("input, textarea").val("");
        $btn = $('form#reused_form').find('button[type="button"]');
        label = $btn.prop('orig_label');
        if(label)
        {
          $btn.prop('type','submit' );
          $btn.text(label);
          $btn.prop('orig_label','');
        }
    };

	$('#reused_form').submit(function(e)
      {
        e.preventDefault();

        $form = $(this);
        //show some response on the button
        $('button[type="submit"]', $form).each(function()
        {
            $btn = $(this);
            $btn.prop('type','button' );
            $btn.prop('orig_label',$btn.text());
            $btn.text('Caricamento ...');
        });

		var values = {};
        $.each($form.serializeArray(), function(i, field) {
            values[field.name] = field.value;
        });

		$.post("addDish.php", {'Title' : values['title'],'Description' : values['desc'],'Price' : values['price'],'idcat' : add_idcat}, function(data) {
        })
        .done(function(data) {
        	$('form#reused_form').hide();
            $('#success_message').show();
            $('#error_message').hide();
            var htmldata = $(data);
            htmldata.insertBefore(add_parent);           
            setTimeout(resetsubmit, 2000);
        })
        .fail(function() {
        	$('#error_message').append('<p>ERRORE INSERIMENTO</p>');

            $('#success_message').hide();
            $('#error_message').show();

            //reverse the response on the button
            $('button[type="button"]', $form).each(function()
            {
                $btn = $(this);
                label = $btn.prop('orig_label');
                if(label)
                {
                    $btn.prop('type','submit' );
                    $btn.text(label);
                    $btn.prop('orig_label','');
                }
            });
        });

      });
});
 
 
(function(){

	var unit = 100;
	var canvas, context, canvas2, context2,height, width, xAxis, yAxis, draw, color;

	function init() {
		canvas = document.getElementById("sineCanvas");
    	color =  canvas.dataset.color;
		canvas.width = document.documentElement.clientWidth; //Canvasのwidthをウィンドウの幅に合わせる
		canvas.height = 300;
		context = canvas.getContext("2d");
		height = canvas.height;
		width = canvas.width;
		xAxis = Math.floor(height/2);
		yAxis = 0;

		draw(color);
	}
	function draw(color){
		context.clearRect(0, 0, width, height);// キャンバスの描画をクリア

		drawWave(color, 1, 3, 0);//波を描画（fillcolor, alpha, zoom, delay）

		// Update the time and draw again
		draw.seconds = draw.seconds + .009;
		draw.t = draw.seconds*Math.PI;
		setTimeout(draw, 35);
	};
	draw.seconds = 0;
	draw.t = 0;

	function drawWave(fillcolor, alpha, zoom, delay) {
		context.fillStyle = fillcolor;
		context.globalAlpha = alpha;

		context.beginPath(); //パスの開始
		drawSine(draw.t / 0.5, zoom, delay);
		context.lineTo(width + 10, height); //パスをCanvasの右下へ
		context.lineTo(0, height); //パスをCanvasの左下へ
		context.closePath() //パスを閉じる
		context.fill(); //塗りつぶす
	}

	function drawSine(t, zoom, delay) {
		var x = t; //時間を横の位置とする
		var y = Math.sin(x)/zoom;
		context.moveTo(yAxis, unit*y+xAxis); //スタート位置にパスを置く

		// Loop to draw segments (横幅の分、波を描画)
		for (i = yAxis; i <= width + 10; i += 10) {
			x = t+(-yAxis+i)/unit/zoom;
			y = Math.sin(x - delay)/3;
			context.lineTo(i, unit*y+xAxis);
		}
	}

	init();

})();
