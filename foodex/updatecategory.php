<?php
session_start();

$servername="localhost";
$username ="foodex";
$password ="";
$database = "my_foodex";

$ID = $_POST['idCat'];
$NOME = $_POST['Nome'];

if(isset($ID) && isset($NOME)){
  	$con = new mysqli($servername, $username, $password, $database);
    $stmt = $con->prepare("UPDATE CATEGORIA SET Nome = ? WHERE IdCategoria = ?");
    $stmt->bind_param("si", $NOME, $ID);
    $result = $stmt->execute();
    if($result){
      echo "Categoria aggiornata";
    } else {
      echo "Errore aggiornamento categoria";
    }

    $stmt->close();
    $con->close();
}
?>