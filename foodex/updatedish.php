<?php
session_start();

$servername="localhost";
$username ="foodex";
$password ="";
$database = "my_foodex";

$ID = $_POST['idDish'];
$TITLE = $_POST['Title'];
$DESC = $_POST['Description'];
$PRICE = $_POST['Price'];

if(isset($ID) && isset($TITLE) && $TITLE != "" && isset($DESC) && isset($PRICE) && $PRICE != ""){
  	$con = new mysqli($servername, $username, $password, $database);
    $stmt = $con->prepare("UPDATE FOOD SET Name = ?, Description = ?, Price = ? WHERE IdFood = ?");
    $stmt->bind_param("ssdi", $TITLE, $DESC, $PRICE, $ID);
    $result = $stmt->execute();
    if($result){
      echo "Menu aggiornato";
    } else {
      die(header("ERRORE"));
    }

    $stmt->close();
    $con->close();
} else{
	die(header("ERRORE"));
}
?>