<?php
session_start();

    if(isset($_SESSION['nome'])){
    	if($_SESSION['Tipologia'] == "Ristorante"){
        	$uploaddir = './restaurantsImages/';
             // $sql = ("UPDATE RESTAURANT SET Image = ? WHERE IdRestaurant = ?");
        } else {
			$uploaddir = './userImages/';
              //$sql = ("UPDATE UTENTE SET Image = ? WHERE Username = ?");
    	}
    }
    
$servername="localhost";
$username ="foodex";
$password ="";
$database = "my_foodex";

    $con = new mysqli($servername, $username, $password, $database);
    
    $stmt = $con->prepare("SELECT MAX(Image) as max FROM RESTAURANT");
    $stmt->execute();
    $result = $stmt->get_result();
    $max = $result->fetch_object();
    echo $max->max;
    


// per prima cosa verifico che il file sia stato effettivamente caricato
if (!isset($_FILES['userfile']) || !is_uploaded_file($_FILES['userfile']['tmp_name'])) {
  echo 'Non hai inviato nessun file...';
  exit;    
}

//controllo su estensione
$ext_ok = array('jpg');
$temp = explode('.', $_FILES['userfile']['name']);
$ext = end($temp);
if (!in_array($ext, $ext_ok)) {
  echo 'Il file ha un estensione non ammessa!';
  exit;
}

//percorso della cartella dove mettere i file caricati dagli utenti
//$uploaddir = './restaurantsImages/';
//$uploaddir = './userImages/';

//Recupero il percorso temporaneo del file
$userfile_tmp = $_FILES['userfile']['tmp_name'];

//recupero il nome originale del file caricato
$userfile_name = $_FILES['userfile']['name'];

$max = $max->max;
$newName = $max + 1;
//copio il file dalla sua posizione temporanea alla mia cartella upload
if (move_uploaded_file($userfile_tmp, $uploaddir . $newName . ".jpg")) {
  //Se l'operazione è andata a buon fine...

$sql = ("UPDATE RESTAURANT SET Image = ? WHERE IdRestaurant = ?");
  if($stmt = $con->prepare($sql)) {
  	$stmt->bind_param('ii', $newName, $_SESSION["IDRest"]);
    if($stmt->execute()){
      $_SESSION["Image"] = $newName;
      header("location: Profile.php");      
    } else {
       echo $stmt->error;
    }
  } else {
       $error = $con->errno . ' ' . $con->error;
   	   echo $error;
  }
}else{
  //Se l'operazione è fallta...
  echo 'Upload NON valido!'; 
}
?>