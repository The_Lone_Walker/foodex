<?php
session_start();

$servername="localhost";
$username ="foodex";
$password ="";
$database = "my_foodex";

$con = new mysqli($servername, $username, $password, $database);

// per prima cosa verifico che il file sia stato effettivamente caricato
if (!isset($_FILES['userfile']) || !is_uploaded_file($_FILES['userfile']['tmp_name'])) {
  echo json_encode(array("a" => 'Non hai inviato nessun file...', "b" => '0')); 
  exit;    
}

//controllo su estensione
$ext_ok = array('jpg');
$temp = explode('.', $_FILES['userfile']['name']);
$ext = end($temp);
if (!in_array($ext, $ext_ok)) {
  echo json_encode(array("a" => 'Il file ha un estensione non ammessa!', "b" => '0')); 
  exit;
}

//percorso della cartella dove mettere i file caricati dagli utenti
$uploaddir = './menuImages/';

//Recupero il percorso temporaneo del file
$userfile_tmp = $_FILES['userfile']['tmp_name'];

//recupero il nome originale del file caricato
$userfile_name = $_FILES['userfile']['name'];

$newName = $_POST["IdFood"];

//copio il file dalla sua posizione temporanea alla mia cartella upload
if (move_uploaded_file($userfile_tmp, $uploaddir . $newName . ".jpg")) {
  //Se l'operazione è andata a buon fine...

  $sql = ("UPDATE FOOD SET Image = ? WHERE IdFood = ?");
  if($stmt = $con->prepare($sql)) {
  	$final = $uploaddir . $newName . '.jpg';
  	$stmt->bind_param('si', $final, $_POST["IdFood"]);
    if($stmt->execute()){
      echo json_encode(array("a" => "Immagine Caricata", "b" => $final));
    } else {
       echo json_encode(array("a" => $stmt->error, "b" => '0'));
    }
  } else {
       $error = $con->errno . ' ' . $con->error;
       echo json_encode(array("a" => $error, "b" => '0'));
  }
}else{
  //Se l'operazione è fallta...
  echo json_encode(array("a" => 'Upload NON valido!', "b" => '0'));
}
?>